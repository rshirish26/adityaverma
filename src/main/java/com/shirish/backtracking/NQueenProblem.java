package com.shirish.backtracking;

public class NQueenProblem {

	public static void main(String[] args) {
		int n = 4;
		boolean [][] board = new boolean[n][n];
		boolean [] column = new boolean[n];
		boolean [] nDiagonal = new boolean[2*n-1];
		boolean [] rDiagonal = new boolean[2*n-1];
		solve(board, 0, column, nDiagonal, rDiagonal, "");

	}

	private static void solve(boolean[][] board, int row, boolean[] column, boolean[] nDiagonal, boolean[] rDiagonal,
			String result) {
		
		if(row == board.length) {
			System.out.println(result);
			return;
		}
		
		for(int col =0; col< board[0].length; col++)
		{
			if(column[col] == false && nDiagonal[row+col] == false && rDiagonal[row-col+board.length-1] == false )
			{
				board[row][col] = true;
				column[col] = true;
				nDiagonal[row+col] = true; 
				rDiagonal[row-col+board.length-1] = true;
				solve(board, row+1, column, nDiagonal, rDiagonal, result +row +"-"+col  + ", ");
				board[row][col] = false;
				column[col] = false;
				nDiagonal[row+col] = false; 
				rDiagonal[row-col+board.length-1] = false;
				
			}
		}
	
		
	}

}
