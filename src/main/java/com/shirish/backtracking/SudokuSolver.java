package com.shirish.backtracking;



public class SudokuSolver {
	
//	3 1 6 5 7 8 4 9 2 
//	5 2 9 1 3 4 7 6 8 
//	4 8 7 6 2 9 5 3 1 
//	2 6 3 4 1 5 9 8 7 
//	9 7 4 8 6 3 1 2 5 
//	8 5 1 7 9 2 6 4 3 
//	1 3 8 9 4 7 2 5 6 
//	6 9 2 3 5 1 8 7 4 
//	7 4 5 2 8 6 3 1 9 


	public static void main(String[] args) {
		int[][] board = { { 3, 0, 6, 5, 0, 8, 4, 0, 0 }, { 5, 2, 0, 0, 0, 0, 0, 0, 0 }, { 0, 8, 7, 0, 0, 0, 0, 3, 1 },
				{ 0, 0, 3, 0, 1, 0, 0, 8, 0 }, { 9, 0, 0, 8, 6, 3, 0, 0, 5 }, { 0, 5, 0, 0, 9, 0, 6, 0, 0 },
				{ 1, 3, 0, 0, 0, 0, 2, 5, 0 }, { 0, 0, 0, 0, 0, 0, 0, 7, 4 }, { 0, 0, 5, 2, 0, 6, 3, 0, 0 }, };

		solveSudoku(board, 0, 0);

	}

	private static void solveSudoku(int[][] board, int i, int j) {
		if (i == board.length) {
			display(board);
			return;
		}
		int ni = 0;
		int nj = 0;
		if (j == board[0].length - 1) {
			ni = i + 1;
			nj = 0;

		} else {
			nj = j + 1;
			ni = i;
		}

		if (board[i][j] != 0) {
			solveSudoku(board, ni, nj);
		} else {
			for (int k = 1; k <= 9; k++) {
				if (isValid(board, i, j, k) == true) {
					board[i][j] = k;
					solveSudoku(board, ni, nj);
					board[i][j] = 0;
				}
			}
		}

	}

	private static boolean isValid(int[][] board, int i, int j, int k) {
		for (int row = 0; row < board.length; row++) {
			if (board[row][j] == k)
				return false;
		}
		for (int col = 0; col < board[0].length; col++) {
			if (board[i][col] == k)
				return false;
		}

		int smi = i / 3 * 3;
		int smj = j / 3 * 3;

		for (int row = 0; row < 3; row++)
			for (int col = 0; col < 3; col++) {
				if (board[smi + row][smj + col] == k)
					return false;
			}
		return true;

	}

	private static void display(int[][] board) {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}

	}

}
