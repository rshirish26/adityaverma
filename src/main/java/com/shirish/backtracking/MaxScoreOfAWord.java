package com.shirish.backtracking;

public class MaxScoreOfAWord {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] words = { "dog", "cat", "dad", "good" };
		char[] letters = { 'a', 'b', 'c', 'd', 'd', 'd', 'g', 'o', 'o' };
		int[] score = { 1, 0, 9, 5, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        //populate the frequency arry
		int[] freq = new int[score.length];
		for (char letter : letters) {
			freq[letter - 'a']++;
		}

		int maxScore = solution(words, freq, score, 0);
		// 23
		System.out.println(maxScore);
	}

	private static int solution(String[] words, int[] freq, int[] score, int index) {

		if (index == words.length) {
			return 0;
		}

		int notIncludedScore = 0 + solution(words, freq, score, index + 1);

		int wordScore = 0;
		String word = words[index];
		boolean flag = true;
		for (int i = 0; i < word.length(); i++) {
			int letter = word.charAt(i);
			if (freq[letter - 'a'] == 0)
				flag = false;

			freq[letter - 'a']--;
			wordScore += score[letter - 'a'];
		}
		int includedScore = 0;
		if (flag) {
			includedScore = wordScore + solution(words, freq, score, index + 1);
			;
		}

		for (int i = 0; i < word.length(); i++) {
			int letter = word.charAt(i);
			freq[letter - 'a']++;
		}

		return Math.max(notIncludedScore, includedScore);
	}

}
