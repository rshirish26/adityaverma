	package com.shirish.backtracking;

import java.util.LinkedList;
import java.util.Queue;

public class GoldMines2 {

	public static void main(String[] args) {
		int [][] goldMines = {
				{20, 30, 0, 1},
				{10, 0,  0, 2},
				{0 ,3 , 6, 5},
				{40, 0, 2, 100}	
		};
		int[][] input = goldMines;
		
		int n = input.length ;
		int m = input[0].length;
		
	   int maxSum = Integer.MIN_VALUE;
		
		for(int i=0; i< n; i++)
		{
			for(int j =0; j< m; j++)
			{
				if(input[i][j] != 0)
				{
					int sum = 0;
					sum += input[i][j] ;
					
					input[i][j]=0;
					Queue<Integer> neighbour = new LinkedList<Integer>();
					neighbour.add(i*m + j);
					while(!neighbour.isEmpty())
					{
						int index = neighbour.remove();
						int row = index / m;
						int col = index % m;
						
						if(row -1 >= 0 && input[row-1][col] !=0)
						{
							neighbour.add((row-1)*m + col);
							sum += input[row-1][col] ;
							input[row-1][col] =0;
						}
						if(row +1 < n && input[row+1][col] !=0)
						{
							neighbour.add((row+1)*m + col);
							sum += input[row+1][col];
							input[row+1][col] =0;
						}
						if(col -1 >= 0 && input[row][col-1] !=0)
						{
							neighbour.add((row)*m + (col-1));
							sum += input[row][col-1];
							input[row][col-1] =0;
						}
						if(col +1 < n && input[row][col+1] !=0)
						{
							neighbour.add((row)*m + col+1);
							sum += input[row][col+1];
							input[row][col+1] =0;
						}
						maxSum = Math.max(sum, maxSum);
						
					}
					
				}
			}
		}
		System.out.println("Maximum gold collected "+maxSum);

	}

}
