package com.shirish.backtracking;

public class PrintAbbrivation {
	
//	pep
//	pe1
//	p1p
//	p2
//	1ep
//	1e1
//	2p
//	3

	public static void main(String[] args) {
		String input = "pep";
		
		solution(input, "", 0, 0);

	}

	private static void solution(String input, String result, int count, int position) {
		
		if(position==input.length())
		{
			if(count>0)
				System.out.println(result + count);
			else 
			    System.out.println(result);
			return;
		}
		
		if(count>0)
			solution(input, result +count+ input.charAt(position), 0, position+1);
		else
		    solution(input, result + input.charAt(position), 0, position+1);
		solution(input, result , count +1, position+1);
		
		
	}

}
