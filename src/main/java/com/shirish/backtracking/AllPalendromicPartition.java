package com.shirish.backtracking;

public class AllPalendromicPartition {

	public static void main(String[] args) {
		String input  = "abaaba";
		solution(input, "");

	}

	private static void solution(String input, String asf) {
	   
		if(input.length() == 0)
		{
			System.out.println("Output : "+ asf);
			return;
		}
		
		for(int i =0; i< input.length(); i++)
		{
			String prefix = input.substring(0,i+1);
			String modified = input.substring(i+1);
			if(isPalindrome(prefix)) {
				solution(modified, asf + "("+prefix+") 	");
			}
		}
		
	}

	private static boolean isPalindrome(String prefix) {
		int i = 0;
		int j = prefix.length()-1;
		while(i< j)
		{
			if(prefix.charAt(i) != prefix.charAt(j))
			{
				return false;
			}
			i++;
			j--;	
		}
		
		return true;
	}

}
