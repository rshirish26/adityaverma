package com.shirish.backtracking;

import java.util.ArrayList;
import java.util.List;

public class PartitionInKSubSet {
	static int counter = 0;

	public static void main(String[] args) {
		int n = 4;
		int k = 3;
		List<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < k; i++) {
			result.add(new ArrayList<Integer>());
		}

		solution(n, k, 0, 1, result);

	}

	private static void solution(int n, int k, int noOfSubset, int index, List<ArrayList<Integer>> result) {
		
		if(index > n)
		{
			if(noOfSubset == k)
			{
				counter++;
				System.out.print(counter +".");
				for(ArrayList<Integer> set : result)
				{
					System.out.print(set + " ");
				}
				System.out.println();
			}
			return;
		}

		for (int i = 0; i < result.size(); i++) {
			if (result.get(i).size() > 0) {
				result.get(i).add(index);
				solution(n, k, noOfSubset, index + 1, result);
				result.get(i).remove(result.get(i).size() - 1);
			} else {
				result.get(i).add(index);
				solution(n, k, noOfSubset + 1, index + 1, result);
				result.get(i).remove(result.get(i).size() - 1);
				break;
			}
		}

	}

}
