package com.shirish.backtracking;

import java.util.HashMap;
import java.util.Map;

public class CryptoArithematic {
	
	// 7-d 8-e 0-m 1-n 3-o 6-r 2-s 5-y
	// 9-d 8-e 0-m 1-n 3-o 6-r 2-s 7-y
	// 2-d 7-e 0-m 1-n 4-o 6-r 3-s 9-y
	// 9-d 7-e 0-m 1-n 4-o 5-r 3-s 6-y
	// 1-d 8-e 0-m 2-n 4-o 6-r 3-s 9-y
	// 9-d 8-e 0-m 2-n 4-o 5-r 3-s 7-y
	// 1-d 7-e 0-m 3-n 6-o 4-r 5-s 8-y
	// 2-d 7-e 0-m 3-n 6-o 4-r 5-s 9-y
	// 9-d 8-e 0-m 4-n 6-o 3-r 5-s 7-y
	// 5-d 4-e 0-m 1-n 7-o 3-r 6-s 9-y
	// 9-d 4-e 0-m 1-n 7-o 2-r 6-s 3-y
	// 4-d 5-e 0-m 2-n 7-o 3-r 6-s 9-y
	// 1-d 8-e 0-m 5-n 7-o 3-r 6-s 9-y
	// 3-d 8-e 0-m 5-n 7-o 2-r 6-s 1-y
	// 6-d 3-e 0-m 1-n 8-o 2-r 7-s 9-y
	// 9-d 4-e 0-m 2-n 8-o 1-r 7-s 3-y
	// 1-d 5-e 0-m 3-n 8-o 2-r 7-s 6-y
	// 4-d 5-e 0-m 3-n 8-o 2-r 7-s 9-y
	// 9-d 5-e 0-m 3-n 8-o 1-r 7-s 4-y
	// 3-d 6-e 0-m 4-n 8-o 2-r 7-s 9-y
	// 9-d 6-e 0-m 4-n 8-o 1-r 7-s 5-y
	// 4-d 3-e 0-m 2-n 9-o 1-r 8-s 7-y
	// 2-d 4-e 0-m 3-n 9-o 1-r 8-s 6-y
	// 2-d 5-e 0-m 4-n 9-o 1-r 8-s 7-y
	// 7-d 5-e 1-m 6-n 0-o 8-r 9-s 2-y


	public static void main(String[] args) {
		String s1 = "send";
		String s2 = "more";
		String s3 = "money";

		String[] inputs = { s1, s2, s3 };
		Map<Character, Integer> mapCharToInteger = new HashMap<Character, Integer>();
		String unique = "";

		for (String input : inputs) {
			for (char c : input.toCharArray()) {
				if (!mapCharToInteger.containsKey(c)) {
					mapCharToInteger.put(c, -1);
					unique += c;
				}
			}
		}
		boolean[] usedStatus = new boolean[10];

		solution(mapCharToInteger, usedStatus, 0, unique, s1, s2, s3);

	}

	private static void solution(Map<Character, Integer> mapCharToInteger, boolean[] usedStatus, int index,
			String unique, String s1, String s2, String s3) {

		if (index == unique.length()) {
			int s1Sum = getSum(mapCharToInteger, s1);
			int s2Sum = getSum(mapCharToInteger, s2);
			int s3Sum = getSum(mapCharToInteger, s3);

			if (s1Sum + s2Sum == s3Sum) {
				print(mapCharToInteger);
			}
			return;
		}

		char letter = unique.charAt(index);
		for (int i = 0; i < 10; i++) {
			if (usedStatus[i] == false) {
				mapCharToInteger.put(letter, i);
				usedStatus[i] = true;
				solution(mapCharToInteger, usedStatus, index + 1, unique, s1, s2, s3);
				usedStatus[i] = false;
				mapCharToInteger.put(letter, -1);
			}
		}

	}

	private static void print(Map<Character, Integer> mapCharToInteger) {
		for (int i = 0; i < 26; i++) {
			char letter = (char) ('a' + i);
			if (mapCharToInteger.containsKey(letter)) {
				System.out.print(mapCharToInteger.get(letter) + "-" + letter + " ");
			}
		}
		System.out.println();
	}

	private static int getSum(Map<Character, Integer> mapCharToInteger, String word) {
		String sum = "";
		for (int i = 0; i < word.length(); i++) {
			sum += mapCharToInteger.get(word.charAt(i));
		}
		return Integer.parseInt(sum);
	}

}
