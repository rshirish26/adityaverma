package com.shirish.backtracking;

public class FriendsPairing {
	
//	(1)(2)(3)
//	(1)(23)
//	(12)(3)
//	(13)

	public static void main(String[] args) {
		int n = 3;
		boolean[] tracker = new boolean[n + 1];

		solution(1, tracker, "", n);

	}

	private static void solution(int index, boolean[] used, String answer, int n) {
		if (index > n) {
			System.out.println(answer);
			return;
		}

		if (used[index] == true) {
			solution(index + 1, used, answer, n); // cross call
		} else {
			used[index] = true;
			solution(index + 1, used, answer + "(" + index + ")", n);  // dot call
			for (int i = index + 1; i <= n; i++) {
				used[i] = true;
				solution(i + 1, used, answer + "(" + index + i + ")", n); // j call
				used[i] = false; 
			}
			used[index]= false;
		}

	}

}
