package com.shirish.backtracking;

public class JosephProblem {

	public static void main(String[] args) {
		int noOfPeople = 5;
		int killNo = 3;
		
		int candidate = solution(noOfPeople, killNo);
		System.out.println(candidate);
		

	}

	private static int solution(int i, int k) {
		if(i==1)
		{
		 return 0;
		}
		int x = solution(i-1, k);
		int y = (x+k)%i;
		return y;
		
	}

}

//0,1