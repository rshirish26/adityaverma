package com.shirish.backtracking;

public class CrossWordProblem {
	
//	+L++++++++
//	+O++++++++
//	+N++++++++
//	+DELHI++++
//	+O+++C++++
//	+N+++E++++
//	+++++L++++
//	++ANKARA++
//	+++++N++++
//	+++++D++++

	public static void main(String[] args) {
		char[][] board = { { '+', '-', '+', '+', '+', '+', '+', '+', '+', '+' },
				{ '+', '-', '+', '+', '+', '+', '+', '+', '+', '+' },
				{ '+', '-', '+', '+', '+', '+', '+', '+', '+', '+' },
				{ '+', '-', '-', '-', '-', '-', '+', '+', '+', '+' },
				{ '+', '-', '+', '+', '+', '-', '+', '+', '+', '+' },
				{ '+', '-', '+', '+', '+', '-', '+', '+', '+', '+' },
				{ '+', '+', '+', '+', '+', '-', '+', '+', '+', '+' },
				{ '+', '+', '-', '-', '-', '-', '-', '-', '+', '+' },
				{ '+', '+', '+', '+', '+', '-', '+', '+', '+', '+' },
				{ '+', '+', '+', '+', '+', '-', '+', '+', '+', '+' }, };
		String input[] = { "DELHI", "ICELAND", "ANKARA", "LONDON" };

		solution(board, input, 0);

	}

	private static void solution(char[][] board, String[] words, int index) {

		if (words.length == index) {
			display(board);
			return;
		}
		String word = words[index];
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] == '-' || word.charAt(0) == board[i][j]) {
					if (canWePutTheWordVertically(board, word, i, j)) {
						boolean[] tracker = putTheWordVertically(board, word, i, j);
						solution(board, words, index + 1);
						removeTheWordVertically(board, tracker, word, i, j);
					}
					if (canWePutTheWordHorizontlly(board, word, i, j)) {
						boolean[] tracker = putTheWordHorizontlly(board, word, i, j);
						solution(board, words, index + 1);
						removeTheWordHorizontlly(board, tracker, word, i, j);
					}
				}
			}
		}

	}

	private static void removeTheWordHorizontlly(char[][] board, boolean[] tracker, String word, int i, int j) {
		for (int jj = 0; jj < word.length(); jj++) {
			if (tracker[jj] == true) {
				board[i][j + jj] = '-';
			}
		}

	}

	private static void removeTheWordVertically(char[][] board, boolean[] tracker, String word, int i, int j) {
		for (int ii = 0; ii < word.length(); ii++) {
			if (tracker[ii] == true) {
				board[i + ii][j] = '-';
			}
		}

	}

	private static boolean[] putTheWordHorizontlly(char[][] board, String word, int i, int j) {
		boolean[] tracker = new boolean[word.length()];
		for (int jj = 0; jj < word.length(); jj++) {
			if (board[i][j + jj] == '-') {
				board[i][j + jj] = word.charAt(jj);
				tracker[jj] = true;
			}
		}
		return tracker;
	}

	private static boolean[] putTheWordVertically(char[][] board, String word, int i, int j) {
		boolean[] tracker = new boolean[word.length()];
		for (int ii = 0; ii < word.length(); ii++) {
			if (board[i + ii][j] == '-') {
				board[i + ii][j] = word.charAt(ii);
				tracker[ii] = true;
			}
		}
		return tracker;
	}

	private static boolean canWePutTheWordHorizontlly(char[][] board, String word, int i, int j) {
		if (j - 1 >= 0 && board[i][j - 1] != '+') {
			return false;
		}
		if (j + word.length() < board[0].length && board[i][j + word.length()] != '+') {
			return false;
		}
		for (int jj = 0; jj < word.length(); jj++) {
			if (j + jj >= board[0].length)
				return false;
			if (board[i][j + jj] == '-' || board[i][j + jj] == word.charAt(jj))
				continue;
			else
				return false;
		}
		return true;
	}

	private static boolean canWePutTheWordVertically(char[][] board, String word, int i, int j) {
		if (i - 1 >= 0 && board[i - 1][j] != '+') {
			return false;
		}
		if (i + word.length() < board.length && board[i + word.length()][j] != '+') {
			return false;
		}
		for (int ii = 0; ii < word.length(); ii++) {
			if (i + ii >= board.length)
				return false;
			if (board[i + ii][j] == '-' || board[i + ii][j] == word.charAt(ii))
				continue;
			else
				return false;
		}
		return true;
	}

	private static void display(char[][] board) {

		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				System.out.print(board[i][j]);
			}
			System.out.println();
		}
	}

}
