package com.shirish.backtracking;

import java.util.HashSet;

public class WordBreak {

	public static void main(String[] args) {
		HashSet<String> set = new HashSet<>();
		set.add("micro");
		set.add("soft");
		set.add("hi");
		set.add("ring");
		set.add("hiring");
		set.add("microsoft");
		String input = "microsofthiring";
		solution(set, input, "");
		
		

	}

	private static void solution(HashSet<String> set, String input, String output) {
		if(input.length() == 0)
		{
			System.out.println(output);
			return;
		}
		
		for(int i =0; i< input.length(); i++)
		{
			String left = input.substring(0, i+1);
			if(set.contains(left)) {
			   String right = input.substring(i+1);
			   solution(set, right, output +" "+ left );	
			}
		}
		
	}

}
