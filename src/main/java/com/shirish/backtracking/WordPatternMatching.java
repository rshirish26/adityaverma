package com.shirish.backtracking;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


public class WordPatternMatching {
//	p -> graph
//	e -> tree

	public static void main(String[] args) {
		
		String str = "graphtreegraph";
		String pattern = "pep";
		Map<Character, String> map = new HashMap<>();
		
		solution(str, pattern, map, pattern);

	}

	private static void solution(String str, String pattern, Map<Character, String> map, String output) {
		
		if(pattern.length() == 0)
		{
			if(str.length() == 0)
			{
				HashSet<Character>  set = new HashSet<>();
				for(int i = 0; i< output.length(); i++)
				{
					char ch = output.charAt(i);
					if(!set.contains(ch))
					{
						System.out.println(ch +" -> "+map.get(ch));
						set.add(ch);
					}
				}
			}
			return;
		}
		
		char character = pattern.charAt(0);
		String rop = pattern.substring(1);
		
		if(map.containsKey(character))
		{
			String alreadyMapped = map.get(character);
			if(str.length() >= alreadyMapped.length())
			{
				String left = str.substring(0, alreadyMapped.length());
				String right = str.substring(alreadyMapped.length());
				if(alreadyMapped.equals(left))
				{
					solution(right, rop, map, output);
				}
			}
			
		} else {
			for(int i =0; i < str.length(); i++)
			{
				String mappingString = str.substring(0, i+1);
				String ros = str.substring(i+1);
				map.put(character, mappingString);
				solution(ros, rop, map,  output );
				map.remove(character);
			}
			
		}
				
		
		
		
	}

}
