package com.shirish.dp.lcs.variation;

/*Longest Repeating Subsequence
Given a string, print the longest repeating subsequence such that the two subsequence don’t have same string character at same position, i.e., any i’th character in the two subsequences shouldn’t have the same index in the original string.
Example:
Input: str = "aab"
Output: "a"
The two subsequence are 'a'(first) and 'a' 
(second). Note that 'b' cannot be considered 
as part of subsequence as it would be at same
index in both.

PROBLEM STATEMENT LINK:https://www.geeksforgeeks.org/longest...

Playlist Link: https://www.youtube.com/watch?v=nqowU...
SHOW LESS*/


public class LongestRepeatingSubSequence {

	public static void main(String[] args) {
		
		 String input = "aabebcdd";
		 
		  
		  //Solution
		  String input1 = input;
		  String input2 = input;
		
		
		int[][] t = new int[input1.length() + 1][input2.length() + 1];

		for (int i = 0; i <= input1.length(); i++) {
			for (int j = 0; j <= input2.length(); j++) {
				if (i == 0 || j == 0)
					t[i][j] = 0;
			}
		}

		for (int i = 1; i <= input1.length(); i++) {
			for (int j = 1; j <= input2.length(); j++) {
				//very very important step
				if (input1.charAt(i - 1) == input2.charAt(j - 1) && i!=j) {
					t[i][j] = 1 + t[i - 1][j - 1];
				} else {
					t[i][j] = Math.max(t[i - 1][j], t[i][j - 1]);
				}
			}
		}
		
		for (int i = 0; i <= input1.length(); i++) {
			System.out.println();
			for (int j = 0; j <= input2.length(); j++) {
					System.out.print(t[i][j] + " ");
			}
		}
		System.out.println("Longest Common Subsequence (3) abd " + t[input1.length()][input2.length()]);
	}

	

}
