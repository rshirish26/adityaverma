package com.shirish.dp.lcs.variation;

public class LongestPalindromicSubsequence {

	public static void main(String[] args) {
	 String input = "agbcba";
	 // String input = "abc";
	  int output =5;
	  
	  //Solution
	  String input1 = input;
	  String input2 = new StringBuffer(input).reverse().toString();
	  int[][] t = new int[input1.length() + 1][input2.length() + 1];

		for (int i = 0; i <= input1.length(); i++) {
			for (int j = 0; j <= input2.length(); j++) {
				if (i == 0 || j == 0)
					t[i][j] = 0;
			}
		}

		for (int i = 1; i <= input1.length(); i++) {
			for (int j = 1; j <= input2.length(); j++) {
				if (input1.charAt(i - 1) == input2.charAt(j - 1)) {
					t[i][j] = 1 + t[i - 1][j - 1];
				} else {
					t[i][j] = Math.max(t[i - 1][j], t[i][j - 1]);
				}
			}
		}
		
		for (int i = 0; i <= input1.length(); i++) {
			System.out.println();
			for (int j = 0; j <= input2.length(); j++) {
					System.out.print(t[i][j] + " ");
			}
		}
		System.out.println("Longest Common Subsequence (6) " + t[input1.length()][input2.length()]);
	}

}
