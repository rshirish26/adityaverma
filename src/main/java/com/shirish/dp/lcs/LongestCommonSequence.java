package com.shirish.dp.lcs;

/*Longest Common Subsequence Problem solution using recursion
Given two sequences, find the length of longest subsequence present in both of them.
A subsequence is a sequence that appears in the same relative order, but not necessarily contiguous. 

For example, “abc”,  “abg”, “bdf”, “aeg”,  ‘”acefg”, .. etc are subsequences of “abcdefg”.

PROBLEM STATEMENT LINK:https://www.geeksforgeeks.org/longest...

Playlist Link: https://www.youtube.com/watch?v=nqowU...


Example 1:

Input: text1 = "abcde", text2 = "ace" 
Output: 3  
Explanation: The longest common subsequence is "ace" and its length is 3.
Example 2:

Input: text1 = "abc", text2 = "abc"
Output: 3
Explanation: The longest common subsequence is "abc" and its length is 3.
Example 3:

Input: text1 = "abc", text2 = "def"
Output: 0
Explanation: There is no such common subsequence, so the result is 0.
 
*/
public class LongestCommonSequence {

	public static void main(String[] args) {
		// TODO Auto-generated metho
		String input1 = "abcdgh";
		String input2 = "abedfh";

		int output = longestCommonSubsequence(input1, input2, input1.length(), input2.length());
		System.out.println("Longest Common Subsequence " + output);

	}
   // Recursive Method
	private static int longestCommonSubsequence(String x, String y, int n, int m) {
		if (n == 0 || m == 0) {
			return 0;
		}
		if (x.charAt(n - 1) == y.charAt(m - 1)) {
			return 1 + longestCommonSubsequence(x, y, n - 1, m - 1);
		} else {
			return Math.max(longestCommonSubsequence(x, y, n, m - 1), longestCommonSubsequence(x, y, n - 1, m));
		}

	}

}
