package com.shirish.dp.lcs;

public class LongestCommonSubsequenceMemo {
	static int[][] t = new int[1000][1000];

	public static void main(String[] args) {

		/*
		 * String input1 = "abcdgh"; String input2 = "abedfh";
		 */

		String input1 = "mhunuzqrkzsnidwbun";
		String input2 = "szulspmhwpazoxijwbq";

		for (int i = 0; i <= input1.length(); i++)
			for (int j = 0; j <= input2.length(); j++)
				t[i][j] = -1;

		int output = longestCommonSubsequence(input1, input2, input1.length(), input2.length());
		System.out.println("Longest Common Subsequence (6) " + output);
	}

	// Memoized Version

	private static int longestCommonSubsequence(String x, String y, int m, int n) {

		if (m == 0 || n == 0) {
			return 0;
		}

		if (t[m][n] != -1)
			return t[m][n];
		if (x.charAt(m - 1) == y.charAt(n - 1)) {
			t[m][n] = 1 + longestCommonSubsequence(x, y, m - 1, n - 1);
			return t[m][n];
		} else {
			t[m][n] = Math.max(longestCommonSubsequence(x, y, m, n - 1), longestCommonSubsequence(x, y, m - 1, n));
			return t[m][n];
		}

	}

}
