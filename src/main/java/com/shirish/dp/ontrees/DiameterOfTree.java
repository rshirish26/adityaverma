package com.shirish.dp.ontrees;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode() {
	}

	TreeNode(int val) {
		this.val = val;
	}

	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}



class DiameteOfTree {
	
	class Res {
		int val;
	}

	static TreeNode root;

	public int diameterOfBinaryTree(TreeNode root) {
		if (root == null)
			return 0;
		Res res = new Res();
		res.val = Integer.MIN_VALUE;
		depth(root, res);
		return res.val - 1;
	}

	public int depth(TreeNode TreeNode, Res res) {
		if (TreeNode == null)
			return 0;
		int L = depth(TreeNode.left, res);
		int R = depth(TreeNode.right, res);
		int temp = Math.max(L, R) + 1;
		int ans = Math.max(temp, L + R + 1);
		res.val = Math.max(res.val, ans);
		return temp;
	}

	public static void main(String args[]) {
		DiameteOfTree tree = new DiameteOfTree();
		tree.root = new TreeNode(-15);
		tree.root.left = new TreeNode(5);
		tree.root.right = new TreeNode(6);
		tree.root.left.left = new TreeNode(-8);
		tree.root.left.right = new TreeNode(1);
		tree.root.left.left.left = new TreeNode(2);
		tree.root.left.left.right = new TreeNode(6);
		tree.root.right.left = new TreeNode(3);
		tree.root.right.right = new TreeNode(9);
		tree.root.right.right.right = new TreeNode(0);
		tree.root.right.right.right.left = new TreeNode(4);
		tree.root.right.right.right.right = new TreeNode(-1);
		tree.root.right.right.right.right.left = new TreeNode(10);
		System.out.println("Max pathSum of the given binary tree is " + tree.diameterOfBinaryTree(root));
	}
}