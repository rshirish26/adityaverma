package com.shirish.dp;

public class SubsetSumProblem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       int [] nums = {2, 3, 7, 8, 10 };
        int sum = 11;
        /*int [] nums = {1, 2, 5 };
        int sum = 4;*/
		/*int [] nums = {-1,-1,1};
        int sum = 0;*/
        System.out.println("Result  "+subsetSumProblem(nums, sum) );
	}
	
public static boolean subsetSumProblem(int[] nums, int k) {
        
        boolean [][] t = new boolean [nums.length+1][k+1];
        for(int i =0; i<= nums.length; i++){
            for(int j = 0; j<= k; j++)
            {
            	if(i==0)
                {
                   t[i][j] = false;        
                }
                if(j==0)
                {
                    t[i][j] = true;
                }
            }
        }
        
        for(int i =1; i<= nums.length; i++){
            for(int j = 1; j<= k; j++)
            {
               if(nums[i-1] <= j)
                {
                    t[i][j] = (t[i-1][j-nums[i-1]])||t[i-1][j]; 
                }
                else{
                    t[i][j]  = t[i-1][j];
                }
            }
        }
        
        
        for(int i=0 ; i<= nums.length; i++)
		{
			System.out.println();
			for(int j=0; j<=k; j++)
			{
				System.out.print(t[i][j] + "  ");
			}
		}
        return t[nums.length][k] ;
        
    }

}
