package com.shirish.dp;

public class MinimumSubsetSumDifference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int nums[] = new int[] {1, 6, 11, 5};
		int result = 1;
		
		int sum = 0;
		for(int i=0; i<nums.length; i++ )
		{
			sum+=nums[i];
		}
		
		int upperBound = sum/2;
	//	List<Integer> resultSet = new ArrayList<Integer>();
		int potentialResult = 0;
		for(int i=upperBound; i>= 0; i++)
		{
			if(isTargetSumPresent(nums, i))
			{
				potentialResult = i;
				break;
			}
		}
		System.out.println("Result  :" +(sum-(2*potentialResult)));

	}
	
	public static boolean isTargetSumPresent(int []nums, int k)
	{
		boolean [][] t = new boolean [nums.length+1][k+1];
        for(int i =0; i<= nums.length; i++){
            for(int j = 0; j<= k; j++)
            {
            	if(i==0)
                {
                   t[i][j] = false;        
                }
                if(j==0)
                {
                    t[i][j] = true;
                }
            }
        }
        
        for(int i =1; i<= nums.length; i++){
            for(int j = 1; j<= k; j++)
            {
               if(nums[i-1] <= j)
                {
                    t[i][j] = (t[i-1][j-nums[i-1]])||t[i-1][j]; 
                }
                else{
                    t[i][j]  = t[i-1][j];
                }
            }
        }
        return t[nums.length][k] ;
        
	}

}
