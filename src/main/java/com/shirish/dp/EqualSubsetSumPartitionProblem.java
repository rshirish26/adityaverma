package com.shirish.dp;

public class EqualSubsetSumPartitionProblem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = { 1, 5, 11, 5 };
		System.out.println("Result  shoud be true:    " + canPartition(nums));
	}
	
	 public static boolean canPartition(int[] nums) {
	        
	        int sum = 0;
	        for (int i=0; i< nums.length; i++)
	        {
	            sum+=nums[i];
	        }
	        
	        if(sum % 2!= 0)
	            return false;
	        
	        boolean [][] t = new boolean[nums.length+1][(sum/2)+1];
	        //base condition
	        int target = sum/2;
	        for(int i =0; i<= nums.length; i++)
	        {
	            for(int j = 0; j<= target; j++)
	                {
	                    if(i==0)
	                    {
	                       t[i][j] = false;        
	                    }
	                    if(j==0)
	                    {
	                        t[i][j] = true;
	                    }
	                }
	        }
	        
	        for(int i =1; i<= nums.length; i++)
	        {
	            for(int j = 1; j<= target; j++)
	                {
	                if(nums[i-1] <= j)
	                {
	                    t[i][j] = t[i-1][j-nums[i-1]] || t[i-1][j];
	                }
	                else{
	                    t[i][j] = t[i-1][j];
	                }
	                
	                }    
	        }
	        return t[nums.length][target];
	 }
}
