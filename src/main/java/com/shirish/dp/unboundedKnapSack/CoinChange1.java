package com.shirish.dp.unboundedKnapSack;

public class CoinChange1 {

	public static void main(String[] args) {
		//Maximum number of ways
		int [] coins= {1, 2, 3};
		int sum = 4;		
        int maximumNumberOfWays = maxNumberOfWays(coins, sum);
        System.out.println("Maximum number of ways  (4) :" +maximumNumberOfWays);
	}
	
	public static int maxNumberOfWays(int [] nums, int k)
	{
		int[][] t = new int[nums.length + 1][k + 1];

		// baseConditionn
		for (int i = 0; i <= nums.length; i++) {
			for (int j = 0; j <= k; j++) {
				if (i == 0 ) {
					t[i][j] = 0;
				}
				if (j == 0)
				{
					t[i][j] = 1;
				}
				
			}
		}
		
		//code 
		for (int i = 1; i <= nums.length; i++) {
			for (int j = 1; j <= k; j++) {

				if (nums[i - 1] <= j) {
					System.out.print("->  "+t[i][j - nums[i - 1]]);
					System.out.println("->  "+t[i - 1][j]);
					t[i][j] = t[i][j - nums[i - 1]] +t[i - 1][j];
				} else {
					t[i][j] = t[i - 1][j];
				}

			}
		}
		
		for(int i=0 ; i<= nums.length; i++)
		{
			System.out.println();
			for(int j=0; j<=k; j++)
			{
				System.out.print(t[i][j] + "  ");
			}
		}
		return t[nums.length][k];
		
	}
	

}
