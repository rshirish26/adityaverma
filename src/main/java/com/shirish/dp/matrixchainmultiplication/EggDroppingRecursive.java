package com.shirish.dp.matrixchainmultiplication;

public class EggDroppingRecursive {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int floor = 5;
		int egg = 3;
		int minimizeAttempt  = solve(floor, egg);
		System.out.println(" minimizeAttempt " +minimizeAttempt);
	}
	
	private static int solve(int f, int e)
	{
		if(f==0 || f==1)
		return f;
		
		if(e ==1)
		{
			return f;
		}
		int minAttempt = Integer.MAX_VALUE;
		
	    for(int k = 1; k<=f; k++)
	    {
	    	int temp = 1+ Math.max(solve(f-1, e-1), solve(f+1, e));
	    	minAttempt = Math.min(minAttempt, temp);
	    	
	    }
		
	    return minAttempt;
		
		
	}

}
