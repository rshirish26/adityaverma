package com.shirish.dp.matrixchainmultiplication;


/*Input: p[] = {40, 20, 30, 10, 30}   
Output: 26000  
There are 4 matrices of dimensions 40x20, 20x30, 30x10 and 10x30.
Let the input 4 matrices be A, B, C and D.  The minimum number of 
multiplications are obtained by putting parenthesis in following way
(A(BC))D --> 20*30*10 + 40*20*10 + 40*10*30

Input: p[] = {10, 20, 30, 40, 30} 
Output: 30000 
There are 4 matrices of dimensions 10x20, 20x30, 30x40 and 40x30. 
Let the input 4 matrices be A, B, C and D.  The minimum number of 
multiplications are obtained by putting parenthesis in following way
((AB)C)D --> 10*20*30 + 10*30*40 + 10*40*30

Input: p[] = {10, 20, 30}  
Output: 6000  
There are only two matrices of dimensions 10x20 and 20x30. So there 
is only one way to multiply the matrices, cost of which is 10*20*30*/

public class MCMRecursive {

	public static void main(String[] args) {
		int [] inputArray = {40, 20, 30, 10, 30}  ;
		int i = 1;
		int j = inputArray.length-1;

		
		int result = mcmRecursive(inputArray, i, j);
		System.out.println("Result is here : "  +result);
	}

	private static int mcmRecursive(int[] inputArray, int i, int j) {
		if(i>=j)
			return 0;
		
		int minimum = Integer.MAX_VALUE;
		for(int k=i ; k<=j-1; k++)
		{
			int temp = mcmRecursive(inputArray, i, k) + mcmRecursive(inputArray, k+1, j) + (inputArray[i-1] * inputArray[k] 
					* inputArray[j]);
			minimum = Math.min(minimum, temp);
		}
		
		return minimum;
	}

}
