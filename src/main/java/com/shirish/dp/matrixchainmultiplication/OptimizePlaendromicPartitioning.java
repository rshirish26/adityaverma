package com.shirish.dp.matrixchainmultiplication;

import java.util.Arrays;

public class OptimizePlaendromicPartitioning {

	public static int[][] t = new int[1001][1001];

	public static void main(String[] args) {
	//	String input = "nitik";
		String input = "abc";

		for (int[] row : t)
			Arrays.fill(row, -1);

		int i = 0;
		int j = input.length() - 1;
		int count = palindromicPartition(i, j, input);
		System.out.println("Count  : " + count);

	}

	private static int palindromicPartition(int i, int j, String input) {

		if (t[i][j] != -1)
			return t[i][j];

		if (i >= j)
			return 0;
		if (isPalindrome(i, j, input))
			return 0;
		int minimum = Integer.MAX_VALUE;
		for (int k = i; k <= j - 1; k++) {
			int temp = 1 + ((t[i][k] == -1) ? palindromicPartition(i, k, input) : t[i][k])
					+ ((t[k+1][j] == -1) ? palindromicPartition(k + 1, j, input) : t[k+1][j]);
			minimum = Math.min(temp, minimum);
		}
		t[i][j] = minimum;
		return t[i][j];
	}

	private static boolean isPalindrome(int i, int j, String input) {
		String substring = null;
		if (j == input.length() - 1)
			substring = input.substring(i);
		else
			substring = input.substring(i, j + 1);

		int leftIndex = 0;
		int rightIndex = substring.length() - 1;

		while (leftIndex < rightIndex) {
			if (substring.charAt(leftIndex) != substring.charAt(rightIndex))
				return false;
			leftIndex++;
			rightIndex--;
		}
		return true;

	}

}
