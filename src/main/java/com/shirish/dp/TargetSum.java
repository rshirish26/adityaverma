package com.shirish.dp;

public class TargetSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] nums = {1,1,1,1,1};
		int S = 3;
		System.out.println("Count " +findTargetSumWays(nums, S) );

	}
	
	  public static int findTargetSumWays(int[] nums, int S) {
	        
	        
	        int range = 0;
	        for(int i =0; i< nums.length; i++)
	        {
	        	range+= nums[i];
	        }
	        System.out.println("RANGE  "+range);
	        
	        int k = (range + S) /2;
	        
	        int count = subsetSumProblem(nums, k);
	        return count;
	    }
	    
	    private static int subsetSumProblem(int[] nums, int k) {
			int[][] t = new int[nums.length + 1][k + 1];

			// baseConditionn
			for (int i = 0; i <= nums.length; i++) {
				for (int j = 0; j <= k; j++) {
					if (i == 0) {
						t[i][j] = 0;
					}
					if (j == 0) {
						t[i][j] = 1;
					}
				}
			}
			// int count = 0;
			// main code
			for (int i = 1; i <= nums.length; i++) {
				for (int j = 1; j <= k; j++) {

					if (nums[i - 1] <= j) {
						t[i][j] = t[i - 1][j - nums[i - 1]] + t[i - 1][j];
					} else {
						t[i][j] = t[i - 1][j];
					}

				}
			}
			
			for(int i=0 ; i<= nums.length; i++)
			{
				System.out.println();
				for(int j=0; j<=k; j++)
				{
					System.out.print(t[i][j] + "  ");
				}
			}
			return t[nums.length][k];
		}



}
