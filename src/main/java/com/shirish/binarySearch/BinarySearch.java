package com.shirish.binarySearch;

public class BinarySearch {

	public static void main(String[] args) {
		int [] array = {1, 2, 3, 4,5, 6, 7, 8, 9, 10};
		int element = 7;
		
		int index = BinarySearch(array, element);
		System.out.println("Result  : "+index);
		

	}

	private static int BinarySearch(int[] array, int element) {
		int start = 0;
		int end = array.length -1;
		while(start <= end)
		{
			int mid  = start + ( (end-start)/2);
			if(element == array[mid])
			{
				return mid;
			}
			else if(element  < array[mid])
			{
				end = mid-1;
			} else 
			{
				start = mid+1;
			}
		}
		
		return -1;
	}

}
