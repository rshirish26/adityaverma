package com.shirish.binarySearch;

public class FirstAndLastOccuranceOfElement {

	public static void main(String[] args) {
		int [] array = {1, 2, 3, 4,4, 4, 4, 8, 9, 10};
		int element = 4;
		
		int indexFirst = BinarySearchFirstOccurance(array, element);
		System.out.println("Result  : "+indexFirst);
		
		int indexLast = BinarySearchLastOccurance(array, element);
		System.out.println("Result  : "+indexLast);
		

	}

	private static int BinarySearchFirstOccurance(int[] array, int element) {
		int start = 0;
		int end = array.length -1;
		int result = -1;
		while(start <= end)
		{
			int mid  = start + ( (end-start)/2);
			if(element == array[mid])
			{
				result = mid;
				end = mid -1;
			}
			else if(element  < array[mid])
			{
				end = mid-1;
			} else 
			{
				start = mid+1;
			}
		}
		
		return result;
	}
	
	
	private static int BinarySearchLastOccurance(int[] array, int element) {
		int start = 0;
		int end = array.length -1;
		int result = -1;
		while(start <= end)
		{
			int mid  = start + ( (end-start)/2);
			if(element == array[mid])
			{ 
				result = mid;
			    start = mid +1;
			}
			else if(element  < array[mid])
			{
				end = mid-1;
			} else 
			{
				start = mid+1;
			}
		}
		
		return result;
	}

}

