package com.shirish.binarySearch;

public class NumberOfTimeASortedArrayIsRotated {

	public static void main(String[] args) {
		// number of times a sorted array is rotated depends up the index of the minimum element
		// 2 which is smaller thna 18 as well as 3
    int array[] = {15, 18, 2, 3, 6, 12,13};
	//	int array[] = {3, 4,2, 5, 1};
		//{1, 2, 3, 4, 5}
	 //Output: 2
     int rotationCount  = getNumberOfTimeASortedArrayIsRotated(array);
     System.out.println("rotationCount " +rotationCount);
     
	}
	
	private static int getNumberOfTimeASortedArrayIsRotated(int[] array) {
		int start = 0;
		int end = array.length -1;
		while(start <= end)
		{
			int mid  = start + ( (end-start)/2);
			int next = mid+1%array.length;
			int prev = (mid+array.length-1 )%array.length;
			if(array[mid] <= array[next] && array[mid] <= array[prev])
			{
				return mid;
			}
		    if(array[mid] >= array[start]) // check for valid cases
			{
		    	start = mid+1;	
			} else if (array[mid] <= array[end])
			{
				end = mid-1;
			}
		}
		
		return -1;
	}


}
