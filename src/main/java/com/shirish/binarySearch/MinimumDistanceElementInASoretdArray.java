package com.shirish.binarySearch;

public class MinimumDistanceElementInASoretdArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] array = {1, 3, 8, 10, 12, 15};
		int element = 13;


		int minimumDifferenceElement = BinarySearch(array, element);
		System.out.println("Result  : "+minimumDifferenceElement);
		

	}

	private static int BinarySearch(int[] array, int element) {
		int start = 0;
		int end = array.length -1;
	
		while(start <= end)
		{
			int mid  = start + ( (end-start)/2);
			if(element == array[mid])
			{
				return array[mid] ;
			}
			else if(element  < array[mid])
			{
			
				end = mid-1;
			} else 
			{
				start = mid+1;
			}
		}
		int lowDiff = Math.abs(element - array[start]);
		int highDiff = Math.abs(element - array[end]);
		return lowDiff<highDiff ? array[start]: array[end];
	}


}
