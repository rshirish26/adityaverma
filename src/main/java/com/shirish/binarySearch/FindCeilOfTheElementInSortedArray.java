package com.shirish.binarySearch;

public class FindCeilOfTheElementInSortedArray {


		public static void main(String[] args) {
			int [] array = {1,2 , 4, 8, 10, 10, 12, 19};
			int element = 5;
			int index = BinarySearch(array, element);
			System.out.println("Result  : "+index);
			

		}

		private static int BinarySearch(int[] array, int element) {
			int start = 0;
			int end = array.length -1;
			int result  =-1;
			while(start <= end)
			{
				int mid  = start + ( (end-start)/2);
				if(element == array[mid])
				{
					
					return mid;
				}
				else if(element  < array[mid])
				{
					 //when moving towards left make sure u store the value for ceil
					result = array[mid];
					end = mid-1;
				} else 
				{
					start = mid+1;
				}
			}
			
			return result;
		}
}
