package com.shirish.binarySearch;

public class SearchInNearlySortedArray {

	public static void main(String[] args) {
			int array[]  = {5, 10, 30, 20, 40};
			 int element = 20;
			 int result = BinarySearch(array, element);
				System.out.println("Result  : "+result);
			

		}

		private static int BinarySearch(int[] array, int element) {
			int start = 0;
			int end = array.length -1;
			while(start <= end)
			{
				int mid  = start + ( (end-start)/2);
				if(element == array[mid])
				{
					return mid;
				}
				if(mid-1 > start && element == array[mid-1] )
				{
					return mid-1;
				}
				if(mid+1 < end && element == array[mid+1] )
				{
					return mid+1;
				}
				else if(element  < array[mid])
				{
					end = mid-2;
				} else 
				{
					start = mid+2;
				}
			}
			
			return -1;
		}
}
