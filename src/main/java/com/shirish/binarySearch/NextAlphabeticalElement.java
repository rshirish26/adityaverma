package com.shirish.binarySearch;

public class NextAlphabeticalElement {

	public static void main(String[] args) {
		//LeetCode
		// TODO Auto-generated method stub
		char [] array = {'a','c', 'f', 'h'};
		char element = 'd';
		char index = BinarySearch(array, element);
		System.out.println("Result  : "+index);
		

	}

	private static char BinarySearch(char[] array, int element) {
		int start = 0;
		int end = array.length -1;
		char result  ='#';
		while(start <= end)
		{
			int mid  = start + ( (end-start)/2);
			if(element == array[mid])
			{
				
				return array[mid];
			}
			else if(element  < array[mid])
			{
				 //when moving towards left make sure u store the value for ceil
				result = array[mid];
				end = mid-1;
			} else 
			{
				start = mid+1;
			}
		}
		return result;
	}
}
