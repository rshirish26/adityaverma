package com.shirish.binarySearch;

import java.util.Arrays;

public class FindAnElementInRotatedSortedArray {

		public static void main(String[] args) {
			// number of times a sorted array is rotated depends up the index of the minimum element
			// 2 which is smaller thna 18 as well as 3
	     int array[] = {15, 18, 2, 3, 6, 12,13};
	     int searchElement = 18;
		 //Output: 2
	     int rotationCount  = getNumberOfTimeASortedArrayIsRotated(array);
	     System.out.println("rotationCount " +rotationCount);
	     
	     
	     int searchInleft = BinarySearch(Arrays.copyOfRange(array, 0, rotationCount), searchElement);
	     int searchInRight = BinarySearch(Arrays.copyOfRange(array, rotationCount, array.length),searchElement );
	     System.out.println("searchInleft " +searchInleft);
	     System.out.println("searchInRight " +searchInRight);
	     
		}
		
		
		
		private static int BinarySearch(int[] array, int element) {
			int start = 0;
			int end = array.length -1;
			while(start <= end)
			{
				int mid  = start + ( (end-start)/2);
				if(element == array[mid])
				{
					return mid;
				}
				else if(element  < array[mid])
				{
					end = mid-1;
				} else 
				{
					start = mid+1;
				}
			}
			
			return -1;
		}

		
		private static int getNumberOfTimeASortedArrayIsRotated(int[] array) {
			int start = 0;
			int end = array.length -1;
			while(start <= end)
			{
				int mid  = start + ( (end-start)/2);
				int next = mid+1%array.length;
				int prev = (mid+array.length-1 )%array.length;
				if(array[mid] <= array[next] && array[mid] <= array[prev])
				{
					return mid;
				}
			    if(array[mid] >= array[start])
				{
			    	start = mid+1;	
				} else if (array[mid] <= array[end])
				{
					end = mid-1;
				}
			}
			
			return -1;
		}


	}

