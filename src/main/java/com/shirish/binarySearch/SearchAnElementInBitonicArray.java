package com.shirish.binarySearch;

import java.util.Arrays;

public class SearchAnElementInBitonicArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] array = {5, 10, 20, 15};
		int element= 10;
		//find peak element;
		int peakIndex = BinarySearch(array);
		System.out.println("Result  : "+peakIndex);
		
		
        
		int indexLeft = BinarySearch(Arrays.copyOfRange(array, 0, peakIndex), element);
		System.out.println("Result  : "+indexLeft);
		int indexRight = BinarySearch(Arrays.copyOfRange(array, peakIndex, array.length), element);
		System.out.println("Result  : "+indexRight);
		

	}

	private static int BinarySearch(int[] array, int element) {
		int start = 0;
		int end = array.length -1;
		while(start <= end)
		{
			int mid  = start + ( (end-start)/2);
			if(element == array[mid])
			{
				return mid;
			}
			else if(element  < array[mid])
			{
				end = mid-1;
			} else 
			{
				start = mid+1;
			}
		}
		
		return -1;
	}

	private static int BinarySearch(int[] array) {
		int start = 0;
		int end = array.length -1;
		while(start <= end)
		{
			int mid  = start + ( (end-start)/2);
			if(mid> 0 && mid < array.length-1)
			{
			if(array[mid-1] < array[mid] && array[mid+1] < array[mid])
			{
				return mid;
			}
			else if(array[mid-1] > array[mid])
			{
				end = mid-1;
			} else if(array[mid+1] > array[mid])
			{
				start = mid+1;
			}
			}
		}
		
		return -1;
	}
	
	


}
