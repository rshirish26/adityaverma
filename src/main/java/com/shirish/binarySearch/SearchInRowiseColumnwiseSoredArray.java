package com.shirish.binarySearch;

public class SearchInRowiseColumnwiseSoredArray {

	public static void main(String[] args) {
		int[][] array ={ {10, 20, 30, 40},
                {15, 25, 35, 45},
                {27, 29, 37, 48},
                {32, 33, 39, 50}};
        int element = 29;

        int[] index = BinarySearch(array, element);
		System.out.println("Result  : "+index[0] + index[1]);
		

	}

	private static int[] BinarySearch(int[][] array, int element) {
		int m = array[0].length;
		int n = array.length;
		int i = 0;
		int j = m -1;
		while(i<n && i>=0 && j<m &&  j>=0)
		{
			
			if(element == array[i][j])
			{
				return new int[] {i,j};
			}
			else if(element  < array[i][j])
			{
				j = j-1;
			} else 
			{
				i = i+1;
			}
		}
		
		return new int[] {-1, -1};
	}

}
