package com.shirish.recursion;

public class TowerOfHanoi {

	public static void main(String[] args) {
		
		 char s = 'S';
		 char h = 'H';
		 char d = 'D';
		 int number =4;
		 
		 solve(number, s, d, h );

	}

	private static void solve(int number, char s, char d, char h) {
		if(number==1)
		{
			System.out.println("Move disk " +number +" from " +s+ "to" +d);
			return;
		}
		solve(number-1, s, h , d);
		System.out.println("Move disk " +number +" from " +s+ "to" +d);
		solve(number-1, h, d , s);
		
	}


}
