package com.shirish.recursion;

public class KthGrammer {

	public static void main(String[] args) {
		// N = 4, K = 5
     int result = kthGrammar( 4, 5);
     System.out.println("Result : "+result  );
	}
	
	public static int kthGrammar(int N, int K) {
        int upper = (int)Math.pow(2, N-1);
        return find(N, K, 1, upper);
    }
    
    private static int find(int height, int target, int startIndex, int endIndex) {
        
        int ans = 0;
        while (height > 1) {
            int mid = startIndex + (endIndex - startIndex) / 2;
            if (target <= mid) {
                endIndex = mid;
            } else {
                if (ans == 0) {
                    ans = 1;
                } else {
                    ans = 0;
                }
                startIndex = mid + 1;
            }
            height--;
        }
        return ans;
    }

}
