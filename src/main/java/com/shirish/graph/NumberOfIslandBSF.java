package com.shirish.graph;

import java.util.LinkedList;
import java.util.Queue;



public class NumberOfIslandBSF {

	public static void main(String[] args) {
		int [][]input =  				{
				  {1,1,0},
				  {1,1,0},
				  {0,0,1}
				};
	/*	int [][]input =  				{
				  {1,1,0,0,0},
				  {1,1,1,0,0},
				  {0,0,1,0,0},
				  {0,0,0,1,1}
				};*/
		int n = input.length ;
		int m = input[0].length;
		
		int numberOfIsland = 0;
		
		for(int i=0; i< n; i++)
		{
			for(int j =0; j< m; j++)
			{
				if(input[i][j] == 1)
				{
					++numberOfIsland;
					input[i][j]=0;
					Queue<Integer> neighbour = new LinkedList<Integer>();
					neighbour.add(i*m + j);
					while(!neighbour.isEmpty())
					{
						int index = neighbour.remove();
						int row = index / m;
						int col = index % m;
						
						if(row -1 >= 0 && input[row-1][col] ==1)
						{
							neighbour.add((row-1)*m + col);
							input[row-1][col] =0;
						}
						if(row +1 < n && input[row+1][col] ==1)
						{
							neighbour.add((row+1)*m + col);
							input[row+1][col] =0;
						}
						if(col -1 >= 0 && input[row][col-1] ==1)
						{
							neighbour.add((row)*m + (col-1));
							input[row][col-1] =0;
						}
						if(col +1 < n && input[row][col+1] ==1)
						{
							neighbour.add((row)*m + col+1);
							input[row][col+1] =0;
						}
						
						
					}
					
				}
			}
		}
		System.out.println("numberOfIsland "+numberOfIsland);
		
	}
}
