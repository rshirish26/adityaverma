package com.shirish.graph;

public class NumberOfIslandDFS {

	public static void main(String[] args) {

	//	int[][] input = { { 1, 1, 0, 0, 0 }, { 1, 1, 0, 0, 0 }, { 0, 0, 1, 0, 0 }, { 0, 0, 0, 1, 1 } };
		int [][]input =  				{
				  {1,1,0},
				  {1,1,0},
				  {0,0,1}
				};
		int n = input.length;
		int m = input[0].length;
		int numberOfIsland = 0;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (input[i][j] == 1) {
					numberOfIsland++;
					dfs(i, j, input);
				}
			}
		}
		System.out.println("Number of Island : " + numberOfIsland);

	}

	private static void dfs(int i, int j, int[][] input) {
		int n = input.length;
		int m = input[0].length;

		if (i < 0 || i >= n || j < 0 || j >= m || input[i][j] == 0) {
			return;
		}
		input[i][j] = 0; //important step

		dfs(i + 1, j, input);
		dfs(i - 1, j, input);
		dfs(i, j + 1, input);
		dfs(i, j - 1, input);

	}

}
