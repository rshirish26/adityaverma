package com.shirish.Stack;

/*Given a binary matrix, find the maximum size rectangle binary-sub-matrix with all 1’s.
Example:

Input :   0 1 1 0
          1 1 1 1
          1 1 1 1
          1 1 0 0

Output :  1 1 1 1
          1 1 1 1*/

public class MaximumAreaOfABinaryMatrix {
	
	

	public static void main(String[] args) {
		int[][] matrix = { { 0, 1, 1, 0 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, { 1, 1, 0, 0 } };
		int input [] = new int[matrix[0].length ];
		for(int j=0 ; j< matrix[0].length ; j++)
		{
			input[j] = matrix[0][j];
		}
		int max = MAH(input);
		for(int i=1; i< matrix.length; i++)
		{
			for(int j =0;j< matrix[0].length ; j++)
			{
				if(matrix[i][j] == 0)
				{	
					input[j] = 0;
				}
				else {
					input[j] = input[j] + matrix[i][j];
				}
			}
		  max= Math.max(max, MAH(input));
		}
	}

	private static int MAH(int[] input) {
		
		return 0;
	}

}
