package com.shirish.Stack;

import java.util.Arrays;
import java.util.Stack;

public class MaximumAreaOfHistogram {

	public static void main(String[] args) {
		int[] input = { 6, 2, 5, 4, 5, 1, 6 };
   //12

		int[] nearestSmallestElementToLeft = getNearestSmallestElementToLeft(input);
		System.out.println("NearestSmallestElementToLeft " + Arrays.toString(nearestSmallestElementToLeft));
		int[] neareastSmallestElementToRight = getNeareastSmallestElementToRight(input);
		System.out.println("NearestSmallestElementToRight " + Arrays.toString(neareastSmallestElementToRight));
		int[] width = new int[input.length];
		for (int i = 0; i < input.length; i++) {
			width[i] = neareastSmallestElementToRight[i] - nearestSmallestElementToLeft[i] -1;
		}
		
		int[] area = new int[input.length];
		for (int i = 0; i < input.length; i++) {
			area[i] = width[i] * input[i];
		}
		
		System.out.println("Area" + Arrays.toString(area));
		

		

	}

	private static int[] getNearestSmallestElementToLeft(int[] input) {
		int[] result = new int[input.length];
		Stack<Entry> stack = new Stack<>();

		for (int i = 0; i < input.length; i++) {
			if (stack.size() == 0) {
				result[i] = -1;
			} else if (stack.size() > 0 && stack.peek().getValue() < input[i]) {
				result[i] = stack.peek().getIndex();
			} else if (stack.size() > 0 && stack.peek().getValue() >= input[i]) {
				while (stack.size() > 0 && stack.peek().getValue() >= input[i]) {
					stack.pop();
				}
				if (stack.size() == 0) {
					result[i] = -1;
				} else {
					result[i] = stack.peek().getIndex();
				}
			}
			stack.add(new MaximumAreaOfHistogram.Entry(i, input[i]));
		}

		

		return result;

	}

	private static int[] getNeareastSmallestElementToRight(int[] input) {
		int[] result = new int[input.length];
		Stack<Entry> stack = new Stack<>();

		for (int i = input.length - 1; i >= 0; i--) {
			if (stack.size() == 0) {
				result[i] = 7;
			} else if (stack.size() > 0 && stack.peek().getValue() < input[i]) {
				result[i] = stack.peek().getIndex();
			} else if (stack.size() > 0 && stack.peek().getValue() >= input[i]) {
				while (stack.size() > 0 && stack.peek().getValue() >= input[i]) {
					stack.pop();
				}
				if (stack.size() == 0) {
					result[i] = -1;
				} else {
					result[i] = stack.peek().getIndex();
				}
			}
			stack.add(new MaximumAreaOfHistogram.Entry(i, input[i]));
		}

		

		return result;
	}

	public static class Entry {

		private int index;

		private int value;

		public Entry(int index, int value) {
			super();
			this.index = index;
			this.value = value;
		}

		public int getIndex() {
			return index;
		}

		public int getValue() {
			return value;
		}

	}

}
