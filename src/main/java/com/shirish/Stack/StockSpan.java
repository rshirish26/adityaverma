package com.shirish.Stack;

import java.util.Arrays;
import java.util.Stack;


/*The stock span problem is a financial problem where we have a series of n daily price quotes for a
 *  stock and we need to calculate span of stock’s price for all n days.
The span Si of the stock’s price on a given day i is defined as the maximum number
 of consecutive days just before the given day, for which the price of the stock on the current 
 day is less than or equal to its price on the given day.
For example, if an array of 7 days prices is given as {100, 80, 60, 70, 60, 75, 85},
 then the span values for corresponding 7 days are {1, 1, 1, 2, 1, 4, 6}
*/
public class StockSpan {
	
	
	
	
	public static void main(String[] args) {
		int[] input = { 100, 80, 60, 70, 60, 75, 85 };
        //{1, 1, 1, 2, 1, 4, 6}
		System.out.println("NearestGreaterElementToTheLeft: " + Arrays.toString(getStockSpan(input)));

	}

	private static int[] getStockSpan(int[] input) {
		int [] result = new int[input.length];
		Stack<Entry> stack = new Stack<>();
		
		for(int i = 0; i < input.length; i++)
		{
			if(stack.size() == 0)
			{
				result[i] = -1;
			}
			else if(stack.size() > 0 && stack.peek().getValue() > input[i])
			{
				result[i] = stack.peek().getIndex();
			}
			else if(stack.size() > 0 && stack.peek().getValue()<= input[i])
			{
				while(stack.size() > 0 && stack.peek().getValue() <= input[i])
				{
					stack.pop();
				}
				if(stack.size() == 0)
				{
					result[i] = -1;
				}
				else {
					result[i] = stack.peek().getIndex();				
				}
			}
			stack.add(new StockSpan.Entry(i, input[i]));
		}
		int [] span = new int[input.length];
		for(int i = 0; i < input.length; i++)
		{
			span[i] = i - result[i];
		}
		
		
		return span;
	}
	
	public static class Entry  {
		   
		private int index;
	    
		private int value;
		
		public Entry(int index, int value) {
			super();
			this.index = index;
			this.value = value;
		}
		
		public int getIndex() {
			return index;
		}

		public int getValue() {
			return value;
		}

	}

}
