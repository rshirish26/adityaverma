package com.shirish.Stack;

import java.util.Arrays;
import java.util.Stack;

public class NextGreaterElement {

	public static void main(String[] args) {
		int [] input  = {4, 5, 2, 25};
		
	//	The nearest greater element to left element : [3, 4, 4, -1]
		System.out.println("The next largest element : " +Arrays.toString(getNextLargestElementArray(input)));
	}

	private static int[] getNextLargestElementArray(int[] input) {
		int [] result = new int [input.length];
		Stack<Integer> stack= new Stack<Integer>();
		for(int i = input.length-1 ; i>=0; i--)
		{
			if(stack.size() == 0)
			{
				result[i] = -1;
			//	stack.push(input[i]);
			}
				
			else if (stack.size() > 0 && stack.peek() > input[i])
			{
				result[i] = stack.peek();
			//	stack.push(input[i]);
			} else if(stack.size() > 0 && stack.peek() <= input[i]) {
				while(!stack.isEmpty() && stack.peek() <= input[i])
				{
					stack.pop();
				}
				if(stack.size() == 0)
				{
					result[i] = -1;
				}
				else 
				{
					result[i] = stack.peek();
				}
			//	stack.push(input[i]);
			}
			stack.push(input[i]);
		}
		
		
		
		return result;
	}

}
