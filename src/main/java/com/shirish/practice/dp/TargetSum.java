package com.shirish.practice.dp;

/*Target Sum Problem
Given a list of non-negative integers, a1, a2, ..., an, and a target, S. Now you have 2 symbols + and -. For each integer, you should choose one from + and - as its new symbol.

Find out how many ways to assign symbols to make sum of integers equal to target S.

Example 1:
Input: nums is [1, 1, 1, 1, 1], S is 3. 
Output: 5
Explanation: 

-1+1+1+1+1 = 3
+1-1+1+1+1 = 3
+1+1-1+1+1 = 3
+1+1+1-1+1 = 3
+1+1+1+1-1 = 3

There are 5 ways to assign symbols to make the sum of nums be target 3.*/


public class TargetSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] nums = {1,1,1,1,1};
		int S = 3;
		System.out.println("Count " +findTargetSumWays(nums, S) );
	/*	
		S1 + S2 = RANGE
	    S1 - S2 = DIFF
	    ---------------
	    2S1 +0 = Range - DIFF
	    S1 = RANGE-DIFF/2
**/
	}
	
	  public static int findTargetSumWays(int[] nums, int S) {
	        
	        
	        int range = 0;
	        for(int i =0; i< nums.length; i++)
	        {
	        	range+= nums[i];
	        }
	        System.out.println("RANGE  "+range);
	        
	        int k = (range + S) /2;
	        
	        int count = subsetSumProblem(nums, k);
	        return count;
	    }
	    
	    private static int subsetSumProblem(int[] nums, int k) {
			int[][] t = new int[nums.length + 1][k + 1];

			// baseConditionn
			for (int i = 0; i <= nums.length; i++) {
				for (int j = 0; j <= k; j++) {
					if (i == 0) {
						t[i][j] = 0;
					}
					if (j == 0) {
						t[i][j] = 1;
					}
				}
			}

			for (int i = 1; i <= nums.length; i++) {
				for (int j = 1; j <= k; j++) {

					if (nums[i - 1] <= j) {
						t[i][j] = t[i - 1][j - nums[i - 1]] + t[i - 1][j];
					} else {
						t[i][j] = t[i - 1][j];
					}

				}
			}
			
			for(int i=0 ; i<= nums.length; i++)
			{
				System.out.println();
				for(int j=0; j<=k; j++)
				{
					System.out.print(t[i][j] + "  ");
				}
			}
			return t[nums.length][k];
		}


}
