package com.shirish.practice.dp.dpOnTrees;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode() {
	}

	TreeNode(int val) {
		this.val = val;
	}

	TreeNode(int val, TreeNode left, TreeNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}



public class DiameterOfBinaryTree {
	class Res {
		int val;
	}

	static TreeNode root;

	public static void main(String[] args) {
		DiameterOfBinaryTree tree = new DiameterOfBinaryTree();
		tree.root = new TreeNode(-15);
		tree.root.left = new TreeNode(5);
		tree.root.right = new TreeNode(6);
		tree.root.left.left = new TreeNode(-8);
		tree.root.left.right = new TreeNode(1);
		tree.root.left.left.left = new TreeNode(2);
		tree.root.left.left.right = new TreeNode(6);
		tree.root.right.left = new TreeNode(3);
		tree.root.right.right = new TreeNode(9);
		tree.root.right.right.right = new TreeNode(0);
		tree.root.right.right.right.left = new TreeNode(4);
		tree.root.right.right.right.right = new TreeNode(-1);
		tree.root.right.right.right.right.left = new TreeNode(10);
		System.out.println("Max pathSum of the given binary tree is " + tree.diameterOfBinaryTree(root));
	}

	private int diameterOfBinaryTree(TreeNode root) {

		if (root == null)
			return 0;
		Res res = new Res();
		res.val = Integer.MIN_VALUE;
		depth(root, res);
		return res.val-1;
	}

	private int depth(TreeNode root, Res res) {
		if(root == null)
			return 0;
		
		int left = depth(root.left, res);
		int right = depth(root.right, res);
		int temp = Math.max(left, right) +1;
		int tempResult = Math.max(temp, left+right+1);
		res.val = Math.max(res.val, tempResult);
		return temp;
		
	}

}
