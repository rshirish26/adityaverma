package com.shirish.practice.dp.dpOnTrees;

class TreeNode2 {
	int val;
	TreeNode2 left;
	TreeNode2 right;

	TreeNode2() {
	}

	TreeNode2(int val) {
		this.val = val;
	}

	TreeNode2(int val, TreeNode2 left, TreeNode2 right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}

public class MaximumPathSumBetween2LeafNodes {
	class Res {
		int val;
	}

	static TreeNode2 root;

	public static void main(String[] args) {
		MaximumPathSumBetween2LeafNodes tree = new MaximumPathSumBetween2LeafNodes();
		tree.root = new TreeNode2(1);
		tree.root.left = new TreeNode2(-2);
		tree.root.right = new TreeNode2(3);

		System.out.println("Max pathSum of the given binary tree is " + tree.MaximumPathSumBetween2LeafNodes(root));
	}

	private int MaximumPathSumBetween2LeafNodes(TreeNode2 root) {

		if (root == null)
			return 0;
		Res res = new Res();
		res.val = Integer.MIN_VALUE;
		depth(root, res);
		// change here
		return res.val;
	}

	private int depth(TreeNode2 root, Res res) {
		if (root == null)
			return 0;
		if(root.left==null && root.right == null)
			return root.val;

		int left = depth(root.left, res);
		int right = depth(root.right, res);

		if(root.left!=null && root.right != null) {
			
		res.val = Math.max(res.val, left + right + root.val);	
		return Math.max(left, right) + root.val;
		}

		return (root.left == null) ? right + root.val 
				: left + root.val; 
	}

}
