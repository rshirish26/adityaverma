package com.shirish.practice.dp.dpOnTrees;

class TreeNode1 {
	int val;
	TreeNode1 left;
	TreeNode1 right;

	TreeNode1() {
	}

	TreeNode1(int val) {
		this.val = val;
	}

	TreeNode1(int val, TreeNode1 left, TreeNode1 right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}

public class MaximumPathSumInTree {
	class Res {
		int val;
	}

	static TreeNode1 root;

	public static void main(String[] args) {
		MaximumPathSumInTree tree = new MaximumPathSumInTree();
		tree.root = new TreeNode1(1);
		tree.root.left = new TreeNode1(-2);
		tree.root.right = new TreeNode1(3);

		System.out.println("Max pathSum of the given binary tree is " + tree.MaximumPathSumInTree(root));
	}

	private int MaximumPathSumInTree(TreeNode1 root) {

		if (root == null)
			return 0;
		Res res = new Res();
		res.val = Integer.MIN_VALUE;
		depth(root, res);
		//change here
		return res.val;
	}

	private int depth(TreeNode1 root, Res res) {
		if (root == null)
			return 0;

		int left = depth(root.left, res);
		int right = depth(root.right, res);
		//change here
		int temp = Math.max(Math.max(left, right) + root.val,root.val);
		//chnage in calculation
		int tempResult = Math.max(temp, left + right + root.val);
		res.val = Math.max(res.val, tempResult);
		//chnage in return
		return tempResult;
	}

}
