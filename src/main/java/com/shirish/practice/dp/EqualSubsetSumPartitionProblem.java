package com.shirish.practice.dp;

/*Equal Sum Partition Problem
Partition problem is to determine whether a given set can be partitioned into two subsets such that the sum of elements in both subsets is same.
Examples:

arr[] = {1, 5, 11, 5}
Output: true 
The array can be partitioned as {1, 5, 5} and {11}*/


public class EqualSubsetSumPartitionProblem {

	public static void main(String[] args) {
		int[] weight = { 1, 5, 11, 5 };
	
		boolean  result = knapsack(weight);
		System.out.println("SubsetSumProblem " + result);

	}

	private static boolean knapsack(int[] wt) {
		int sum = 0;
		for(int w : wt)
		{
			sum +=w; 
		}
		if(sum%2!=0)
			return false;
		
		int w = sum/2;

		boolean [][] t = new boolean[wt.length+1][w+1];
		for(int i=0; i< wt.length+1; i++)
			for(int j=0; j< w+1; j++)
			{
				if(i==0)
                {
                   t[i][j] = false;        
                }
                if(j==0)
                {
                    t[i][j] = true;
                }
			}
		
	

		for(int i=1; i<  wt.length+1; i++)
		{
		    for(int j =1; j< w+1; j++)
		    {
		    	if(wt[i-1] <= j)
		    	{
		    		t[i][j] = (t[i-1][j-wt[i-1]] || t[i-1][j]);
		    	
		    	}
		    	else {
		    		t[i][j] = t[i-1][j];
		    
		    	}
		    }
		}
		for(int i=0; i< wt.length+1; i++)
		{
			for(int j=0; j< w+1; j++)
			{
				System.out.print(t[i][j] +" ");
			}
			System.out.println();
		}
		
		return t[wt.length][w];
	}

}
