package com.shirish.practice.dp.unbounded;

public class CoinChangeMax {

/*	Coin Change Problem Maximum Number of ways
	Given a value N, if we want to make change for N cents, and we have infinite supply of each of S = { S1, S2, .. , Sm} valued coins, how many ways can we make the change? The order of coins doesn’t matter.
	Example:
	for N = 4 and S = {1,2,3}, there are four solutions: {1,1,1,1},{1,1,2},{2,2},{1,3}. So output should be 4.*/
	public static void main(String[] args) {
		//Maximum number of ways
		int [] coins= {2,3,6,7};
		int sum = 7;		
        int maximumNumberOfWays = maxNumberOfWays(coins, sum);
        System.out.println("Maximum number of ways  (4) :" +maximumNumberOfWays);
	}
	
	public static int maxNumberOfWays(int [] nums, int k)
	{
		int[][] t = new int[nums.length + 1][k + 1];

		// baseConditionn
		for (int i = 0; i <= nums.length; i++) {
			for (int j = 0; j <= k; j++) {
				if (i == 0 ) {
					t[i][j] = 0;
				}
				if (j == 0)
				{
					t[i][j] = 1;
				}
				
			}
		}
		
		//code 
		for (int i = 1; i <= nums.length; i++) {
			for (int j = 1; j <= k; j++) {

				if (nums[i - 1] <= j) {
					System.out.print("->  "+t[i][j - nums[i - 1]]);
					System.out.println("->  "+t[i - 1][j]);
					t[i][j] = t[i][j - nums[i - 1]] +t[i - 1][j];
				} else {
					t[i][j] = t[i - 1][j];
				}

			}
		}
		
		for(int i=0 ; i<= nums.length; i++)
		{
			System.out.println();
			for(int j=0; j<=k; j++)
			{
				System.out.print(t[i][j] + "  ");
			}
		}
		return t[nums.length][k];
		
	}
	
}
