package com.shirish.practice.dp.unbounded;

/*Rod Cutting Problem
Given a rod of length n inches and an array of prices that contains prices of all pieces of size smaller than n. Determine the  maximum value obtainable by cutting up the rod and selling the pieces. 
Example: 
if length of the rod is 8 and the values of different pieces are given as following, then the maximum obtainable value is 22 (by cutting in two pieces of lengths 2 and 6)


length   | 1   2   3   4   5   6   7   8  
--------------------------------------------
price    | 1   5   8   9  10  17  17  20*/

public class RodCuttingProblem {

	public static void main(String[] args) {
		int [] length = {1, 2, 3, 4, 5, 6, 7, 8};
		int [] price = {1, 5, 8, 9, 10, 17, 17, 20};
		int N= 8;
		int maximizeTheProfit = maximizeTheProfit(length, price, N);
		System.out.println("Maximize the price (22): " + maximizeTheProfit);

	}

	private static int maximizeTheProfit(int[] nums, int[] price, int k) {
        
		int[][] t = new int[nums.length + 1][k + 1];

		// baseConditionn
		for (int i = 0; i <= nums.length; i++) {
			for (int j = 0; j <= k; j++) {
				if (i == 0 || j==0) {
					t[i][j] = 0;
				}
				
			}
		}
		
		//code 
		for (int i = 1; i <= nums.length; i++) {
			for (int j = 1; j <= k; j++) {

				if (nums[i - 1] <= j) {
					//only change is here with Respect to knapsack
					t[i][j] = Math.max (price[i-1] + t[i][j - nums[i - 1]] , t[i - 1][j]);
				} else {
					t[i][j] = t[i - 1][j];
				}

			}
		}
		
		for(int i=0 ; i<= nums.length; i++)
		{
			System.out.println();
			for(int j=0; j<=k; j++)
			{
				System.out.print(t[i][j] + "  ");
			}
		}
		return t[nums.length][k];
		
	
	}

}
