package com.shirish.practice.dp;

/*Sum of subset differences
Given a set of integers, the task is to divide it into two sets S1 and S2 such that the absolute difference between their sums is minimum.
If there is a set S with n elements, then if we assume Subset1 has m elements, Subset2 must have n-m elements and the value of abs(sum(Subset1) – sum(Subset2)) should be minimum.

Example:
Input:  arr[] = {1, 6, 11, 5} 
Output: 1
Explanation:
Subset1 = {1, 5, 6}, sum of Subset1 = 12 
Subset2 = {11}, sum of Subset2 = 11 
*/
public class MinimumSubsetSumDifference {

	public static void main(String[] args) {
	int[] weight = {1, 6, 11, 5} ;

	int sum = 0;
	for(int i=0; i< weight.length; i++)
	{
		sum+=weight[i];
	}
	
	int upperBound = sum/2;
	int potentialResult = 0;
	for(int i = upperBound; i>=0;i--)
	{
		if(knapsack(weight, i))
		{
			potentialResult=i;
			break;
		}
	}

	
	System.out.println("MinimumSubsetSumDifference "+(sum-(2* potentialResult)));

}

	private static boolean knapsack(int[] wt, int w) {

		boolean[][] t = new boolean[wt.length + 1][w + 1];
		for (int i = 0; i < wt.length + 1; i++)
			for (int j = 0; j < w + 1; j++) {
				if (i == 0) {
					t[i][j] = false;
				}
				if (j == 0) {
					t[i][j] = true;
				}
			}

		for (int i = 1; i < wt.length + 1; i++) {
			for (int j = 1; j < w + 1; j++) {
				if (wt[i - 1] <= j) {
					t[i][j] = (t[i - 1][j - wt[i - 1]] || t[i - 1][j]);

				} else {
					t[i][j] = t[i - 1][j];

				}
			}
		}
		for (int i = 0; i < wt.length + 1; i++) {
			for (int j = 0; j < w + 1; j++) {
				System.out.print(t[i][j] + " ");
			}
			System.out.println();
		}

		return t[wt.length][w];
	}

}
