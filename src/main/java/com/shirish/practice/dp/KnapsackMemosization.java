package com.shirish.practice.dp;

import java.util.Arrays;

public class KnapsackMemosization {

	static int[][] t = new int[102][1002];

	public static void main(String[] args) {
		int[] value = { 60, 100, 120 };
		int[] weight = { 10, 20, 30 };
		int knapsackCap = 50;

		for (int[] e : t) {
			Arrays.fill(e, -1);
		}

		int maximizeProfit = knapsack(value, weight, knapsackCap, value.length);
		System.out.println("Maximum Profit " + maximizeProfit);

	}

	private static int knapsack(int[] val, int[] wt, int w, int n) {

		if (n == 0 || w == 0)
			return 0;

		if (t[n][w] != -1)
			return t[n][w];

		if (wt[n - 1] <= w) {
			t[n][w] = Math.max(val[n - 1] + knapsack(val, wt, w - wt[n - 1], n - 1), knapsack(val, wt, w, n - 1));
			return t[n][w];
		} else {
			t[n][w] = knapsack(val, wt, w, n - 1);
			return t[n][w];
		}

	}
}
