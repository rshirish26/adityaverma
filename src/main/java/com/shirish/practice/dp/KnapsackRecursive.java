package com.shirish.practice.dp;

public class KnapsackRecursive {

	public static void main(String[] args) {
		int [] value = {60, 100, 120};
		int [] weight = {10, 20, 30};
		int knapsackCap = 50;
		
		int maximizeProfit = knapsack(value, weight, knapsackCap, value.length);
		System.out.println("Maximum Profit "+maximizeProfit );
		
	}

	private static int knapsack(int[] val, int[] wt, int w, int n) {
		if(n==0 || w==0)
			return 0;
		
		if(wt[n-1] <= w)
		{
			return Math.max(val[n-1]+knapsack(val, wt, w- wt[n-1], n-1), knapsack(val, wt, w, n-1));
		} else 
		{
			return knapsack(val, wt, w, n-1);
		}

	}

}
