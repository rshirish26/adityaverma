package com.shirish.practice.dp;

/*Count of subsets sum with a Given sum
Given an array arr[] of length N and an integer X, the task is to find the number of subsets with sum equal to X.
Example:

Input: arr[] = {1, 2, 3, 3}, X = 6
Output: 3
All the possible subsets are {1, 2, 3},
{1, 2, 3} and {3, 3}*/


public class CountOfSubsetSumWithAGivenSum {

	public static void main(String[] args) {
		int[] weight = {1, 2, 3, 3};
		int knapsackCap = 6;
		
		int  result = knapsack(weight, knapsackCap);
		System.out.println("SubsetSumProblem " + result);

	}

	private static int knapsack(int[] wt, int w) {

		int [][] t = new int[wt.length+1][w+1];
		//base condition
		for(int i=0; i< wt.length+1; i++)
			for(int j=0; j< w+1; j++)
			{
				if(i==0)
                {
                   t[i][j] = 0;        
                }
                if(j==0)
                {
                    t[i][j] = 1;
                }
			}
		
	
        
		for(int i=1; i<  wt.length+1; i++)
		{
		    for(int j =1; j< w+1; j++)
		    {
		    	if(wt[i-1] <= j)
		    	{
		    		t[i][j] = (t[i-1][j-wt[i-1]]) + (t[i-1][j]);
		    	
		    	}
		    	else {
		    		t[i][j] = t[i-1][j];
		    
		    	}
		    }
		}
		for(int i=0; i< wt.length+1; i++)
		{
			for(int j=0; j< w+1; j++)
			{
				System.out.print(t[i][j] +" ");
			}
			System.out.println();
		}
		
		return t[wt.length][w];
	}


}
