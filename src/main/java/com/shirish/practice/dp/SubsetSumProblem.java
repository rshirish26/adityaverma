	package com.shirish.practice.dp;

/*Subset Sum Problem(Dynamic Programming)
Given a set of non-negative integers, and a value sum, determine if there is a subset of the given set with sum equal to given sum.
Example:

Input:  set[] = {3, 34, 4, 12, 5, 2}, sum = 9
Output:  True  //There is a subset (4, 5) with sum 9.
*/
public class SubsetSumProblem {

	public static void main(String[] args) {
		int[] weight = {3, 34, 4, 12, 5, 2};
		int knapsackCap = 9;
		
		boolean  result = knapsack(weight, knapsackCap);
		System.out.println("SubsetSumProblem " + result);

	}
	
	
	/*
	 * true false false false false false false false false false 
       true false false true false false false false false false 
       true false false true false false false false false false 
       true false false true true false false true false false 
       true false false true true false false true false false 
       true false false true true true false true true true 
       true false true true true true true true true true 
	 */

	private static boolean knapsack(int[] wt, int w) {

		boolean [][] t = new boolean[wt.length+1][w+1];
		for(int i=0; i< wt.length+1; i++)
			for(int j=0; j< w+1; j++)
			{
				if(i==0)
                {
                   t[i][j] = false;        
                }
                if(j==0)
                {
                    t[i][j] = true;
                }
			}
		
	

		for(int i=1; i<  wt.length+1; i++)
		{
		    for(int j =1; j< w+1; j++)
		    {
		    	if(wt[i-1] <= j)
		    	{
		    		t[i][j] = (t[i-1][j-wt[i-1]] || t[i-1][j]);
		    	
		    	}
		    	else {
		    		t[i][j] = t[i-1][j];
		    
		    	}
		    }
		}
		for(int i=0; i< wt.length+1; i++)
		{
			for(int j=0; j< w+1; j++)
			{
				System.out.print(t[i][j] +" ");
			}
			System.out.println();
		}
		
		return t[wt.length][w];
	}


}
