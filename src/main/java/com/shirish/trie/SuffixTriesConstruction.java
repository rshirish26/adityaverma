package com.shirish.trie;

import java.util.HashMap;
import java.util.Map;

public class SuffixTriesConstruction {

	public static void main(String[] args) {
		String input = "visible";
		Program.SuffixTrie trie1 = new Program.SuffixTrie("invisible");
		System.out.println("result : " +trie1.contains(input));
	}
	//i n v i s i b l e
	// n v i s i b l e
	// v i s i ble
}

class Program {

	static class TrieNode {
		Map<Character, TrieNode> children = new HashMap<Character, TrieNode>();
	}

	static class SuffixTrie {
		TrieNode root = new TrieNode();
		char endSymbol = '*';

		public SuffixTrie(String str) {
			populateSuffixTrieFrom(str);
		}

		//dont need to do this if you want to create a suffix tree
		public void populateSuffixTrieFrom(String str) {
		   for(int i =0; i< str.length(); i++)
		   {
			   insertSubStringStartingAt(i, str);
		   }
		}

		public void insertSubStringStartingAt(int i, String str) {
			 TrieNode node = root;
		     for(int j=i ; j < str.length(); j++)
		     {
		    	  char letter = str.charAt(j);
		    	  if(!node.children.containsKey(letter))
		    	  {
		    		  TrieNode newNode = new TrieNode();
		    		  node.children.put(letter, newNode);
		    	  }
		    	  node = node.children.get(letter);
		     }
		     node.children.put(endSymbol, null);
		}
		
		public boolean contains(String str)
		{
			TrieNode node = root;
			for(int i=0; i< str.length(); i++)
			{
				char letter = str.charAt(i);
				if(!node.children.containsKey(letter))
					return false;
				node = root.children.get(letter);
			}
			
			return node.children.containsKey(endSymbol)? true :false;
			
		}

	
	}

}
