package com.shirish.heap;

import java.util.Arrays;
import java.util.PriorityQueue;



public class KClosestNumber {

	public static void main(String[] args) {
		int [] array = {5, 6, 7, 8, 9};
		int numberOfClosestElements = 3;
		int closestToNumber = 7;
		System.out.println("K closest element : "+Arrays.toString(getKClosestNumber(array, numberOfClosestElements, closestToNumber)));

	}
	private static int[] getKClosestNumber(int[] input, int k, int x) {
		PriorityQueue<Entry> maxheap = new PriorityQueue<>();
		int [] result = new int[3];
		int index = 0;
		for(int value : input)
		{
			maxheap.add(new KClosestNumber.Entry(x-value, value));
			if(maxheap.size() > k)
			{
				maxheap.poll();
				
			}		
		}	
		while(!maxheap.isEmpty())
		{
			result[index] = maxheap.poll().getValue();
			index++;
		}		
		return result;
	}
	
	public static class Entry implements Comparable<Entry> {
	   
		private int key;
	    private int value;

	    public Entry(int key, int value) {
	        this.key = key;
	        this.value = value;
	    }

	    // getters

	    @Override
	    public int compareTo(Entry other) {
	        return other.getKey() - this.getKey();
	    }
	    
	    public int getKey() {
			return key;
		}

		public void setKey(int key) {
			this.key = key;
		}

		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}

	}

}
