package com.shirish.heap;

import java.util.PriorityQueue;

public class KSmallestElementInTheArray {

	public static void main(String[] args) {
		int[] array = { 7, 10, 4, 3, 20, 15 };
		int k = 3;
		int result = getKSmallestElementInTheArray(array, k);
		System.out.println("KSmallestElementInTheArray " + result);

	}
	
	// smallest number  - max heap

	private static int getKSmallestElementInTheArray(int[] array, int k) {
		
		//max heap mai sab se maximum upar rahega
		
		PriorityQueue maxHeap = new PriorityQueue(3, new MyComparator());
		for (int i = 0; i < array.length; i++) {
			maxHeap.add(array[i]);

			if (maxHeap.size() > k) {
				maxHeap.poll();
			}

		}
		return (int) maxHeap.peek();
	}

}
