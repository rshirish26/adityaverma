package com.shirish.heap;

public class Point implements Comparable<Point> {
	
	

	private int [] points;
	
	private int distance;
	@Override
	public int compareTo(Point o) {
		// TODO Auto-generated method stub
		return o.distance - this.distance;
	}
	
	public Point(int[] points, int distance) {
		super();
		this.points = points;
		this.distance = distance;
	}

	
	public int[] getPoints() {
		return points;
	}

	public int getDistance() {
		return distance;
	}
}
