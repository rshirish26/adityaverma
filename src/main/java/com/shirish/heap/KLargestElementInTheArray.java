package com.shirish.heap;

import java.util.PriorityQueue;

public class KLargestElementInTheArray {

	public static void main(String[] args) {
		int[] array = { 7, 10, 4, 3, 20, 15 };
		int k = 3;
		int result = getKLargestElementInTheArray(array, k);
		System.out.println("KSmallestElementInTheArray " + result);

	}

	private static int getKLargestElementInTheArray(int[] array, int k) {
		
		//min heap mai sab se minimum upar rahega

		PriorityQueue minHeap = new PriorityQueue();
		for (int i = 0; i < array.length; i++) {
			minHeap.add(array[i]);

			if (minHeap.size() > k) {
				minHeap.poll();
			}

		}
		return (int) minHeap.peek();
	}

}
		