package com.shirish.heap;

import java.util.Arrays;
import java.util.PriorityQueue;

public class KClosestPointsToOrigin {
	
	public static void main(String[] args) {
		int[][] array = { {1, 3}, {-2,2}, {5,8}, {0,1} };
		int k = 2;


		System.out.println("K closest element : " + Arrays.toString(getKClosestPointsToOrigin(array, k)));

	}

	private static int[] getKClosestPointsToOrigin(int[][] array, int k) {
	
		PriorityQueue<Point> maxHeap = new PriorityQueue<>();
		
		for(int [] point : array)
		{
			maxHeap.add(new Point(new int[] {point[0], point[1]}, (int)Math.sqrt(Math.pow(point[0], 2) +Math.pow(point[1], 2) )));
			if(maxHeap.size() > k)
			{
				maxHeap.poll();
			}
		}

		return maxHeap.poll().getPoints();
	}

}
