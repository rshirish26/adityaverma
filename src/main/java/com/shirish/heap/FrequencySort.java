package com.shirish.heap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class FrequencySort {

	public static void main(String[] args) {
		int[] array = { 1, 2, 1, 4, 1, 2, 3 };


		System.out.println("K closest element : " + Arrays.toString(frequencySort(array)));

	}

	private static int[] frequencySort(int[] nums) {

		Map<Integer, Integer> count = new HashMap();
		for (int n : nums) {
			count.put(n, count.getOrDefault(n, 0) + 1);
		}
		int [] result = new int [nums.length];
		
		PriorityQueue<Event> minHeap = new PriorityQueue<Event>();
		for(int key : count.keySet())
		{ 
			System.out.println(key);
			System.out.println( count.get(key));
			minHeap.add(new Event(key, count.get(key)));
		}
		int countN=0;
		while(!minHeap.isEmpty())
		{
			Event e = minHeap.poll();
			int frequency = e.getFreq();
			int value = e.getValue();
			for(int i =0; i< frequency; i++)
			{
				result[countN] = value;
				countN++;
			}
			
		}
		
      return result;
        
	}
}
