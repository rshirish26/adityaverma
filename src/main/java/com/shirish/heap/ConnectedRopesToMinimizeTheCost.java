package com.shirish.heap;

import java.util.PriorityQueue;
import java.util.Queue;

public class ConnectedRopesToMinimizeTheCost {

	public static void main(String[] args) {
		int [] array = {1, 2, 3, 4, 5};
		
		Queue  minHeap = new PriorityQueue<>();
		for(int value : array)
		{
			minHeap.add(value);
		}
		int cost = 0;
		while(minHeap.size() >=2)
		{
			int first = (int) minHeap.peek();
			minHeap.poll();
			int second  =  (int) minHeap.peek();
			minHeap.poll();
			cost = first+ second;
			minHeap.add(first+ second);
		}
		System.out.println("Cost " + cost);
		

	}

}
