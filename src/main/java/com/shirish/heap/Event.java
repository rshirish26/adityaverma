package com.shirish.heap;



public class Event implements Comparable<Event>{

	private int value;

	private int freq;

	
	
	public Event(int value, int freq) {
		super();
		this.value = value;
		this.freq = freq;
	}
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getFreq() {
		return freq;
	}

	public void setFreq(int freq) {
		this.freq = freq;
	}

	@Override
	public int compareTo(Event o) {
		// TODO Auto-generated method stub
		return o.freq - this.freq;
	}

}
