package com.shirish.heap;

import java.util.Arrays;
import java.util.PriorityQueue;

public class SortKSortedArray {

	public static void main(String[] args) {
		int [] array = {6,5,3,2,9,8,10};
		int k = 3;
		int[] result = getSortKSortedArray(array, k);
		System.out.println("KSmallestElementInTheArray " + Arrays.toString(result));

	}

	private static int[] getSortKSortedArray(int[] array, int k) {

		PriorityQueue minHeap = new PriorityQueue();
		int [] result = new int [array.length];
		int index = 0;
		for (int i = 0; i < array.length; i++) {
		
			if (minHeap.size()== k+1) {
				result[index] = (int) minHeap.poll();
			 index++;
			}
			minHeap.add(array[i]);

		}
		
		while(!minHeap.isEmpty())
		{
			result[index] = (int) minHeap.poll();
			index++;
		}
		return result;
	}


}
