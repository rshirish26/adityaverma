package com.shirish.slidingWindow;
/*
Share
Given an array of n positive integers and a positive integer s, find the minimal length of a contiguous subarray of which the sum ≥ s. If there isn't one, return 0 instead.

Example: 

Input: s = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: the subarray [4,3] has the minimal length under the problem constraint.
Follow up:
If you have figured out the O(n) solution, try coding another solution of which the time complexity is O(n log n). */

public class MinimumSizeSubArrraySum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public int minSubArrayLen(int sum, int[] input) {
		int left = 0;
		int result = Integer.MAX_VALUE;

		int sumTillNow = 0;

		for (int i = 0; i < input.length; i++) {
			int current = input[i];
			sumTillNow += current;

			while (sumTillNow >= sum) {
				result = Math.min(result, i - left + 1);
				sumTillNow = sumTillNow - input[left];
				left++;
			}
		}

		return result != Integer.MAX_VALUE ? result : 0;

	}

}
