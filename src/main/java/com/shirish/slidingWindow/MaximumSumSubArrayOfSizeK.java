package com.shirish.slidingWindow;

public class MaximumSumSubArrayOfSizeK {

	public static void main(String[] args) {
		int [] input = {100, 200, 300, 400};
		int windowSize = 2;
		int maxSum = getMaximumSumSubArrayOfSizeK(input, windowSize);
		System.out.println("Maximum Sum : "+maxSum);
	}

	private static int getMaximumSumSubArrayOfSizeK(int[] input, int k) {
		int i = 0;
		int j = 0;
		int sum =0;
		int maxSum = Integer.MIN_VALUE;
		while(j < input.length)
		{
			//do the sum // just bcoz you are counting here so this
			sum += input[j];
			//check if the window size is exceeding
			if(j-i+1 < k)
			{	
				j++;
			} // when windo size is same as given
			else if(j-i+1 == k)
			{
				//check if the sum si greater than the max sun
				maxSum = Math.max(sum, maxSum);
				//substract first numbers sum
				sum = sum -input[i];
				// increment both the counters
				
				j++;
				i++;
			}
		}
		
		return maxSum;
	}

}
