package hackerrank;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Linked {

	public static void main(String[] args) {
		List<Integer>  input = new ArrayList<Integer>();
		input.add(2);
		input.add(1);
		input.add(3);
		input.add(1);
		input.add(4);
		input.add(1);
		input.add(3);
		
		List<Integer> result = sub(input);
		System.out.println(result);
		

	}

	private static List<Integer> sub(List<Integer> input) {
		
		List<Integer>  output = new ArrayList<Integer>();
		LinkedHashMap<Integer, Integer> lhm = new LinkedHashMap<Integer, Integer>(); 
		for(int number : input)
		{
			if(lhm.containsKey(number))
			{
				int count = lhm.get(number);
				lhm.put(number, count+1);
				continue;
			}
			lhm.put(number, 1);
			output.add(number);
		}
		
		
		return output;
	}

}
