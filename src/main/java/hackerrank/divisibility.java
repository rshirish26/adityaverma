package hackerrank;

public class divisibility {

	public static void main(String[] args) {
		String s= "rbrb";
		String t = "rbrb";
        int result = findSmallest(s, t);
        System.out.println(result);
	}

	private static int findSmallest(String str1, String str2) {
		int m = str1.length(), n = str2.length(), d = gcd(m, n);
        String s = str1.substring(0, d), str = str1 + str2;
        for (int i = 0; i < m + n; i += d) {
            if (!str.startsWith(s, i)) 
                return 0;        
        }
       return s.length();
	
	}
	
	private static int gcd(int a, int b) {
        return a == 0 ? b : gcd(b % a, a);
    }

}
