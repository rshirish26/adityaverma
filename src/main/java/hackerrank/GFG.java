package hackerrank;
//hacgrouperrangroup
public class GFG {

	public static void main(String[] args) {

		int N = 200;
		int group = 111;

		System.out.print(countWaystoDivide(N, group));
	}

	static int [][][]dp = new int[500][500][500];
	  
	static int numberOfGroups(int pos, int prev, 
	                     int people, int groups)
	{

	    if (pos == groups) 
	    {
	        if (people == 0)
	            return 1;
	        else
	            return 0;
	    }
	   

	    if (people == 0)
	        return 0;
	  
	    if (dp[pos][prev][people] != -1)
	        return dp[pos][prev][people];
	  
	    int answer = 0;

	    for (int i = prev; i <= people; i++) 
	    {
	        answer += numberOfGroups(pos + 1, i, 
	                           people - i, groups);
	    }
	  
	    return dp[pos][prev][people] = answer;
	}
	  

	static int countWaystoDivide(int n, int groups)
	{
	        for (int i = 0; i < 201; i++) 
	        {
	            for (int j = 0; j < 201; j++)
	            {
	                for (int l = 0; l < 201; l++)
	                    dp[i][j][l] = -1;
	            }
	        }
	  
	    return numberOfGroups(0, 1, n, groups);
	}

	
}
