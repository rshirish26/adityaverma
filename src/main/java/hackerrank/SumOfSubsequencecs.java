package hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class SumOfSubsequencecs {

	static List<String> al = new ArrayList<>();
	 
    // Creating a public static Arraylist such that
    // we can store values
    // IF there is any question of returning the
    // we can directly return too// public static
    // ArrayList<String> al = new ArrayList<String>();
    public static void main(String[] args)
    {
        String s = "50678";
        findsubsequences(s, ""); // Calling a function
        System.out.println(al);
        List<Integer> prime = new ArrayList<>();
        for(int i=0 ; i < al.size(); i++)
        {
        	if(!al.get(i).equals("")) {
        	int number = Integer.parseInt(al.get(i));
        	System.out.println(number);
        	if(isPrime(number))
        	{
        		prime.add(number);
        	}
        	}
        	
        }
        
        Collections.sort(prime);
        prime.get(prime.size()-1);
        System.out.println("Greatest Prime numner : "+prime.get(prime.size()-1));
    }
 
    private static void findsubsequences(String s,
                                         String ans)
    {
        if (s.length() == 0) {
            al.add(ans);
            return;
        }
 
        // We add adding 1st character in string
        findsubsequences(s.substring(1), ans + s.charAt(0));
 
        // Not adding first character of the string
        // because the concept of subsequence either
        // character will present or not
        findsubsequences(s.substring(1), ans);
    }
    
    static boolean isPrime(int n)
    {
        // Corner case
        if (n <= 1)
            return false;
  
        // Check from 2 to n-1
        for (int i = 2; i < n; i++)
            if (n % i == 0)
                return false;
  
        return true;
    }


}
