package hackerrank;

import java.util.ArrayList;
import java.util.List;

public class NumberOfTimeASortedArrayIsRotated {
	
	public static void main(String[] args) {
		
		int array[] = {3, 4,2, 5, 1};
		List<Integer>  input = new ArrayList<Integer>();
		input.add(5);
		//input.add(1);
		input.add(2);
		input.add(6);
		input.add(4);
		input.add(3);
     int rotationCount  = getNumberOfTimeASortedArrayIsRotated(input);
	//	int count = minDeletions(input);
     //System.out.println("rotationCount " +count);
     System.out.println("rotationCount2 " +rotationCount);
     
	}
	
	private static int getNumberOfTimeASortedArrayIsRotated(List<Integer>  input) {
		
		int start = 0;
		int end = input.size() -1;
		while(start <= end)
		{
			int mid  = start + ( (end-start)/2);
			System.out.println("mid" +mid);
			int next =(mid+1)%input.size();
			System.out.println("next" +next);
			
			int prev = (mid+input.size()-1 )%input.size();
			if(input.get(mid) <= input.get(next) && input.get(mid)  <= input.get(prev))
			{
				
				return input.get(mid);
			}
	    if(input.get(mid)  >= input.get(start)) // check for valid cases
			{
		    	start = mid+1;	
			} else if (input.get(mid) <= input.get(end))
			{
				end = mid-1;
			}
	}
		
		return -1;
	}
	
//	public static int minDeletions(List<Integer> arr) {
//	       int number = 0;
//	     
//	       int count = 0;
//	       List<Integer>  input = new ArrayList<Integer>();
//	       input = arr;
//	   
//	      while(number != -1)
//	       {
//	          
//	           number = getNumberOfTimeASortedArrayIsRotated(input);
//	           if(number!= -1) {
//	                System.out.println(number);
//	               count++;
//	           input.remove(number);
//	           }
//	       }
//	       
//	       return count -1;
//
//	    }
//	    
//	    private static int getNumberOfTimeASortedArrayIsRotated(List<Integer>  input) {
//	        System.out.println("Size" + input.size());
//	        int start = 0;
//	        int end = input.size() -1;
//	        while(start <= end)
//	        {
//	            int mid  = start + ( (end-start)/2);
//	            int next = mid+1%input.size();
//	            int prev = (mid+input.size()-1 )%input.size();
//	            if(input.get(mid) <= input.get(next) && input.get(mid)  <= input.get(prev))
//	            {
//	                
//	                return input.get(mid);
//	            }
//	            if(input.get(mid)  >= input.get(start)) // check for valid cases
//	            {
//	                start = mid+1;    
//	            } else if (input.get(mid) <= input.get(end))
//	            {
//	                end = mid-1;
//	            }
//	        }
//	        
//	        return -1; 
//	    }


}
